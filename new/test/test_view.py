from django.test import TestCase, Client
from new.models import ResourceDetail, ProjectDetail
from django.urls import reverse
# Create your tests here.

#
# class ResourceViewTest(TestCase):
#     def create_resource(self, first_name='abc', email_id='abc@wooshelf.com', empolyee_id=2):
#         return ResourceDetail.objects.create(first_name=first_name, email_id=email_id, empolyee_id=empolyee_id)
#
#     def test_resource_detail(self):
#         res_detl = self.create_resource(first_name='abc')
#         detail_url = reverse(r"^viewResource/$", kwargs={'id': res_detl.empolyee_id})
#         response = self.client.get(detail_url)
#         self.assertTrue(response.status_code, 2)


class ResourceViewTest(TestCase):
    def setUp(self):
        self.resource = Client()
        self.list_url = reverse('viewResource')
        # self.detail_url = reverse('viewResource/update/(?P<id>\\d+)/$', args=['resource1'])
        # self.detail_url = reverse('viewResource/delete/(?P<id>\d+)/$')
        # self.resource1 = ResourceDetail.objects.create(first_name='Priyanshu', email_id='Shah@gmail.com', employee_id=2)

    def test_resource_list(self):
        resource = ResourceDetail.objects.create(first_name='Priyanshu', email_id='Shah@gmail.com', empolyee_id=2)
        response = self.resource.get('/viewResource/%d/' % (resource.empolyee_id,))
        self.assertEquals(response.status_code, 404)
        self.assertTemplateUsed(response, 'templates/new/resource/viewResource.htmls')

#     def test_resource_details(self):
#         response = self.resource.post(self.detail_url)
#         self.assertEquals(response.status_code, 302)
#         self.assertTemplateUsed(response, 'new/resource/update.html')
#
#     def test_resource_delete(self):
#         response = ResourceDetail.objects,create(first_name='delete', last_name='shah', email_id='prince@gmail.com', employee_id=10)
#         response = self.client.delete(self.detail_url)
#         self.assertEquals(response.status_code, 202)
#
#     def test_resource_create(self):
#         url = reverse('addResource')
#         response = self.client.post(url, {
#             'first_name': 'priyanshu',
#             'last_name': 'shah',
#             'email_id': 'a@gmail.com'
#         })
#         resource2 = ResourceDetail.objects.get(id=2)
#         self.assertEquals(resource2.first_name, 'priyanshu')
# #
# class ResourceViewTest(TestCase):
#     def create_resource(self, first_name='abc', email_id='abc@wooshelf.com', empolyee_id=2):
#         return ResourceDetail.objects.create(first_name=first_name, email_id=email_id, empolyee_id=empolyee_id)
#
#     def test_resource_detail_view(self):
#         res_detl = self.create_resource(first_name='abc')
#         detail_url = reverse(['viewResource/$'], kwargs={'id': res_detl.empolyee_id})
#         response = self.client.get(detail_url)
#         self.assertTrue(response.status_code, 200)


class ProjectViewTest(TestCase):
    def test_project_list(self):
        client = Client()
        response = self.client.post(reverse('view_projects'))
        self.assertEquals(response.status_code, 302)
        self.assertTemplateUsed(response, 'new/project/viewprojects.html')
