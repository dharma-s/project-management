from django.db import models
from django.contrib.auth.models import User
from new.models import *


class Notification(models.Model):
    notification = models.CharField(max_length=300)
    notification_type = models.CharField(max_length=300)
    admin_receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name="admin_notification", null=True,
                                       blank=True)
    resource_receiver = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE,
                                          related_name="resource_notification", null=True, blank=True)
    manager_receiver = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE,
                                         related_name="manager_notification", null=True, blank=True)
    client_receiver = models.ForeignKey(ClientDetail, on_delete=models.CASCADE, related_name="client_notification",
                                        null=True, blank=True)
    status = models.BooleanField()
    admin_read_status = models.BooleanField(default=False)
    resource_read_status = models.BooleanField(default=False)
    manager_read_status = models.BooleanField(default=False)
    client_read_status = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.notification

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-created_at', '-updated_at']


