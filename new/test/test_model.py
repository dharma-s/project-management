from django.test import TestCase
from new.models import ResourceDetail, ClientDetail, ProjectDetail, ResourceProjectDetail, RaisedQueryDetail, LeadModel, Board
# Create your tests here.


class ResourceDetailTest(TestCase):
    def test_resource_detail(self):
        resource = ResourceDetail.objects.create(first_name='abc', email_id='abc@wooshelf.com', technology='Python', empolyee_id=1)
        resource.last_name = "Shah"
        resource.password = ""
        resource.save()
        self.assertEquals(ResourceDetail.objects.filter(email_id='abc@wooshelf.com').count(), 1)
        self.assertEquals(resource.last_name, "Shah")
        self.assertEquals(resource.technology, "Python")


# This Unit Testing For ClientDetail Models
class ClientDetailTest(TestCase):
    def test_client_detail(self):
        client = ClientDetail.objects.create(first_name="Prince", email_id='client@wooshelf.com', contact='9144882339')
        client.position = "1st Class"
        client.save()
        self.assertEquals(ClientDetail.objects.filter(email_id='client@wooshelf.com').count(), 1)
        self.assertEquals(client.first_name, 'Prince')
        self.assertEquals(client.contact, '9144882339')


# This Unit Testing For ProjectDetail Models
class ProjectDetailTest(TestCase):
    def test_project_detail(self):
        client = ClientDetail.objects.create(first_name="Prince", email_id='client3@wooshelf.com', contact=9144882339)
        project = ProjectDetail.objects.create(name="Wooshelf", client=client)
        project.project_raised_query()
        project.resource_detail()
        project.resource_list()
        project.get_raisedquery_list()
        project.get_project_rqd()
        self.assertEquals(ProjectDetail.objects.filter(name="Wooshelf", client=client).count(), 1)
        self.assertEquals(project.name, 'Wooshelf')
        self.assertEquals(project.client.first_name, 'Prince')


# This Unit Testing For ResourceProjectDetail Models
class ResourceProjectDetailTest(TestCase):
    def test_resource_project_detail(self):
        resource = ResourceDetail.objects.create(first_name='abc', email_id='abc1@wooshelf.com', technology='Python', empolyee_id=2)
        client = ClientDetail.objects.create(first_name="Prince", email_id='client@wooshelf.com', contact=9144882339)
        project = ProjectDetail.objects.create(name="Wooshelf", client=client)
        rpd = ResourceProjectDetail.objects.create(resource=resource, project=project)
        rpd.get_resource()
        rpd.get_project()
        rpd.get_attend_list()
        self.assertEquals(rpd.resource.first_name, 'abc')
        self.assertEquals(rpd.project.name, 'Wooshelf')


# This Unit Testing For RaisedQueryDetail Models
class RaisedQueryDetailTest(TestCase):
    def setUp(self):
        pass
    def raised_query_detail(self):
        resource = ResourceDetail.objects.create(first_name='abc', email_id='abc5@wooshelf.com', technology='Python', empolyee_id=5)
        client = ClientDetail.objects.create(first_name="Prince", email_id='client5@wooshelf.com', contact=9144882339)
        project = ProjectDetail.objects.create(name="Wooshelf", client=client)
        rqd = RaisedQueryDetail.objects.create(resource=resource, project=project)
        rqd.description = "Raised Query Details Demo"
        rqd.save()
        self.assertEquals(RaisedQueryDetail.objects.filter(resource=resource).count(), 2)


class LeadModelTest(TestCase):
    def lead_model_query(self):
        resource = ResourceDetail.objects.create(first_name='abc', email_id='abc1@wooshelf.com', technology='Python',
                                                 empolyee_id=5)
        lead = LeadModel.objects.create(title="Lead", description='nothing', added_by=resource)
        lead.get_lead_followUP()
        lead.get_status()
        lead.get_client()
        self.assertEquals(LeadModel.objects.filter(added_by=ResourceDetail.objects.create(email_id='prince@gmail.com')).count(), 1)
        self.assertEquals(lead.resource, 'Lead')


# This Unit Testing For Board Models
class ResourceBoardTest(TestCase):
    def resource_board_test(self):
        resource1 = ResourceDetail.objects.create(first_name='resourceBoard', email_id='resBoard10@wooshelf.com', empolyee_id=111)
        client1 = ClientDetail.objects.create(first_name="client10", email_id='client110@wooshelf.com', contact=9144882356)
        project1 = ProjectDetail.objects.create(name="pms", client=client1)
        board1 = Board.objects.create(project=project1, name="Board10", description="Every thing is fine")
        resource_board = ResourceBoard.objects.create(resource=resource1, board=board1, role='QA')
        self.assertEquals(resource_board.resource.first_name, 'resourceBoard')
        self.assertEquals(resource_board.board.name, 'Board10')

