$(document).on('click', '#viewLeadView', function(e) {
    var lead_id = $(this).data('lead-view-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: { 'lead_id': lead_id, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
        dataType: 'json',
        beforeSend: function() {
            $("#popmodelView").modal("show");
        },
        success: function(data) {
            console.log(data)
            $("#popmodelView .modal-body").html(data.html);
        }
    });
});