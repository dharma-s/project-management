from rest_framework import serializers
from .models import Blog,JobApplicant,WebQueries, Subscriber


class blogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Blog
        fields = "__all__"


class applicantSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobApplicant
        fields = "__all__"


class querySerializer(serializers.ModelSerializer):
    class Meta:
        model = WebQueries
        fields = "__all__"


class subscriberSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscriber
        fields = "__all__"

