from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils.six import text_type

class AppTokenGenerator(PasswordResetTokenGenerator):
    
    """ This class is used to generate token with the spacific
    
    timestamp to reset password """

    def _make_hash_value(self,user,timestamp):
      print(text_type(user.is_active)+text_type(user.email_id)+text_type(timestamp))
      return (text_type(user.is_active)+text_type(user.email_id)+text_type(timestamp))

token_generator = AppTokenGenerator()     
