from django.contrib import admin
from salary_management.models import *
# Register your models here.


admin.site.register(Salary)
admin.site.register(EmployeeAccountDetail)
admin.site.register(EmployeeSalary)
