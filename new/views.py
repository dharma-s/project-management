from datetime import date, timedelta, datetime
import datetime
from genericpath import exists
from django.contrib.messages.api import MessageFailure
from django.db.models import query
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from .models import *
from .forms import *
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets, status, permissions
from .serializers import blogSerializer, applicantSerializer, querySerializer, subscriberSerializer
from rest_framework.response import Response
from django.http import FileResponse, HttpResponse, JsonResponse, request
from django.contrib.auth.decorators import user_passes_test
import json
from django.views.decorators.csrf import csrf_exempt
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from django.core.mail import EmailMessage, send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text, DjangoUnicodeDecodeError
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from notification_app.views import add_task_notification_id, update_task_notification, task_end_date_notification, \
    add_lead_notification, update_lead_notification


def index(request):
    context = {}
    role = get_user_role(request, request.user)

    if role == "superadmin":
        today_leaves = 0
        present_empolyees = 0
        not_marked_empolyees = 0
        new_dict = {}
        total_empolyees = ResourceDetail.objects.all()
        for empolyee in total_empolyees:
            status = get_attend_status(empolyee)
            if status == "Present":
                present_empolyees += 1
            elif status == "Leave":
                today_leaves += 1
            elif status == "Not Marked":
                not_marked_empolyees += 1

        try:
            last_four_leads = LeadModel.objects.all()[0:4]
        except IndexError:
            last_four_leads = LeadModel.objects.all()
        try:
            four_active_projects = ProjectDetail.objects.filter(status="Active")[0:4]
        except IndexError:
            four_active_projects = ProjectDetail.objects.filter(status="Active")
        try:
            ten_recent_worklogs = Worklog.objects.all()[0:4]
        except IndexError:
            ten_recent_worklogs = Worklog.objects.all()
        try:
            recent_ten_attendence = Attend.objects.all()[0:4]
        except IndexError:
            recent_ten_attendence = Attend.objects.all()

        context["last_four_leads"] = last_four_leads
        context["four_active_projects"] = four_active_projects
        context["ten_recent_worklogs"] = ten_recent_worklogs
        context["recent_ten_attendence"] = recent_ten_attendence
        context["title"] = "Dashboard"
        new_dict["today_leaves"] = today_leaves
        new_dict["total_empolyees"] = total_empolyees.count()
        new_dict["present_empolyees"] = present_empolyees
        new_dict["not_marked_empolyees"] = not_marked_empolyees
        new_dict = json.dumps(new_dict)
        context["new_dict"] = new_dict
        # context["pre_month_attend_count"] = pre_month_attend_count(request, ResourceDetail.objects.all())
        return render(request, "new/index.html", context=context)

    elif role == "client":
        client_projects = ProjectDetail.objects.filter(client=request.user)
        return render(request, "new/index.html", {"client_projects": client_projects, "title": "Dashboard"})

        return render(request, "new/index.html", context=context)

    elif role in ["business_development_execute", "lead_generator"]:
        resource = ResourceDetail.objects.get(email_id=request.user.email_id)
        try:
            leads = LeadModel.objects.all()[:4]
        except:
            leads = LeadModel.objects.all()
        context["leads"] = leads
        context["title"] = "Dashboard"
        context["resource_attendance"] = get_attend_status(request.user)
        context["progress"] = get_progress(request)
        context["resource"] = resource
        context["pre_month_attend_count"] = pre_month_res_attend_count(request.user)
        return render(request, "new/index.html", context)

    elif role in ["project_manager", "solution_architect", "frontend_team_lead", "backend_team_lead",
                  'business_analyst']:
        resource = ResourceDetail.objects.get(email_id=request.user.email_id)
        try:
            project_list = ResourceProjectDetail.objects.filter(resource=resource,
                                                                project__in=ProjectDetail.objects.filter(
                                                                    status="Active"))[:4]
        except:
            project_list = ResourceProjectDetail.objects.filter(resource=resource,
                                                                project__in=ProjectDetail.objects.filter(
                                                                    status="Active"))
        try:
            worklog = Worklog.objects.filter(resource=resource)[:10]
        except:
            worklog = Worklog.objects.filter(resource=resource)
        lead_project = ResourceProjectDetail.objects.filter(resource=resource).values('project')
        total_members = ResourceProjectDetail.objects.filter(project__in=lead_project).count()

        resources = ResourceProjectDetail.objects.filter(project__in=lead_project).distinct('resource')
        res = ResourceProjectDetail.objects.filter(project__in=lead_project).values('resource')
        present_member = Attend.objects.filter(date=datetime.date.today(), resource__in=res).count()
        leave_count = 0
        for res in resources:
            if get_today_resource_leave(res.resource):
                leave_count += 1

                # method start to get total resource, Leave, Present project wise - (Rajendra)
        try:
            lead_project_list = ResourceProjectDetail.objects.filter(resource=resource)[:3]
        except:
            lead_project_list = ResourceProjectDetail.objects.filter(resource=resource)

        pres_res_count = []
        leave_res_count = []
        proj_res_count = []
        project_name = []

        for pr in lead_project_list:
            resources_on_proj = ResourceProjectDetail.objects.filter(project=pr.project)
            proj_res = ResourceProjectDetail.objects.filter(project=pr.project).values('resource')
            pres_m = Attend.objects.filter(date=datetime.date.today(), resource__in=proj_res,
                                           project=pr.project).count()
            lc = prc = 0
            for r in resources_on_proj:
                prc += 1
                if get_today_resource_leave(r.resource):
                    lc += 1

            pres_res_count.append(pres_m)
            leave_res_count.append(lc)
            proj_res_count.append(prc)
            project_name.append(pr.project)

        context["pres_res_count"] = pres_res_count
        context["leave_res_count"] = leave_res_count
        context["proj_res_count"] = proj_res_count
        context["project_name"] = project_name
        # end 

        context["resource_projects"] = project_list
        context["resource_worklog"] = worklog
        context["total_members"] = total_members
        context["members_present"] = present_member
        context["members_leave"] = leave_count
        context["resource_attendance"] = get_attend_status(request.user)
        context["progress"] = get_progress(request)
        context["title"] = "Dashboard"
        context["resource"] = resource
        context["pre_month_attend_count"] = pre_month_res_attend_count(request.user)
        return render(request, "new/index.html", context)

    elif role == "resource_member":
        resource = ResourceDetail.objects.get(email_id=request.user.email_id)
        try:
            project_list = ResourceProjectDetail.objects.filter(resource=resource,
                                                                project__in=ProjectDetail.objects.filter(
                                                                    status="Active"))[:4]
        except:
            project_list = ResourceProjectDetail.objects.filter(resource=resource,
                                                                project__in=ProjectDetail.objects.filter(
                                                                    status="Active"))
        try:
            worklog = Worklog.objects.filter(resource=resource)[:4]
        except:
            worklog = Worklog.objects.filter(resource=resource)

        context["resource_projects"] = project_list
        context["resource_worklog"] = worklog
        context["resource_attendance"] = get_attend_status(request.user)
        context["progress"] = get_progress(request)
        context["title"] = "Dashboard"
        context["resource"] = resource
        context["pre_month_attend_count"] = pre_month_res_attend_count(request.user)

        return render(request, "new/index.html", context)
    elif role == 'human_resource':
        total_present = 0
        total_not_mark = 0
        total_leave = 0
        total_resource = ResourceDetail.objects.all()
        total_present_resource_list = []
        total_leave_resource_list = []
        not_mark = []
        for resource in total_resource:
            status = get_attend_status(resource)
            if status == "Present":
                total_present_resource_list.append(resource)
                total_present += 1
            elif status == "Leave":
                total_leave_resource_list.append(resource)
                total_leave += 1
            elif status == "Not Marked":
                not_mark.append(resource)
                total_not_mark += 1

        context["total_resource"] = total_resource.count()
        context["total_present"] = total_present
        context["total_leave"] = total_leave
        context["total_not_mark"] = total_not_mark
        context["present_resource"] = total_present_resource_list
        context["today_leave"] = total_leave_resource_list
        context["not_mark"] = not_mark

        return render(request, "new/index.html", context)
    else:
        return render(request, "new/index.html", {"title": "Dashboard"})


def get_email(request):
    """This method is used to get email """

    try:
        current_user_email = request.user.email_id
    except AttributeError:
        current_user_email = request.user.email
    return current_user_email


def user_login(request):
    """This view will log the user in """

    if request.user.is_authenticated:
        return redirect('index')

    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["password"]
        user = authenticate(username=email, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, "Welcome to wooshelf project management system! ")
            return redirect("index")
        else:
            messages.warning(request, "Invaild credentials! ")
            return redirect("login")
    return render(request, "new/users/login.html")


def viewFile(request, projectname):
    """This View is returning associated file of project"""
    try:
        project = ProjectDetail.objects.get(name=projectname)
        filepath = project.files.path
        project_file = open(filepath, "rb")
        return FileResponse(project_file)
    except ProjectDetail.DoesNotExist:
        return HttpResponse("Does Not Exist")


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def addProject(request):
    if request.method == "POST":
        form = ProjectDetailForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully added!")
            return redirect("view_projects")
        else:
            return render(request, "new/project/addProject.html", context={"form": form, "title": "Add Project"})
    else:
        form = ProjectDetailForm()
        return render(request, "new/project/addProject.html", context={"form": form, "title": "Add Project"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def viewProjects(request):
    list_of_projects = ProjectDetail.objects.all()
    return render(request, "new/project/viewprojects.html",
                  {"list_of_projects": list_of_projects, "title": "View Projects"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def alterProject(request, id):
    instance = get_object_or_404(ProjectDetail, id=id)
    form = ProjectDetailForm(request.POST or None, request.FILES or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully Changed! ")
            return redirect("view_projects")
        else:
            messages.warning(request, "Invaid details! ")
            return redirect("view_projects")
    return render(request, "new/project/update.html", {"form": form, "title": "Update Project"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def deleteProject(request, id):
    instance = get_object_or_404(ProjectDetail, id=id)
    if instance is not None:
        ProjectDetail.delete(instance)
        messages.success(request, "Successfully deleted!")
        return redirect("view_projects")
    else:
        messages.warning(request, "Project not found!")
        return redirect("view_projects")


# Client details models
@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin", "lead_generator"],
                  login_url="/unauthorized/")
def addClient(request):
    if request.method == "POST":
        client_data = ClientDetailForm(request.POST, request.FILES)
        email = request.POST.get("email_id")
        if client_data.is_valid():
            client_data.save()
            messages.success(request, "Successfully added client.. !")
            role = get_user_role(request, request.user)
            if role == "lead_generator":
                return redirect("add_lead")
            else:
                return redirect("view_client")
        else:

            return render(request, "new/client/addClient.html", {"form": client_data, "title": "Add Project"})
    else:
        form = ClientDetailForm()
        return render(request, "new/client/addClient.html", {"form": form, "title": "Add Project"})


def addClientLead(request):
    if request.method == "POST":
        client_data = ClientDetailForm(request.POST, request.FILES)
        email = request.POST.get("email_id")
        if client_data.is_valid():
            client_data.instance.created_by = str(request.user.id)
            client_data.save()
            messages.success(request, "Successfully added client.. !")
            return redirect("add_lead")
        else:
            messages.debug(request, "Incorrect details!")
            return render(request, "new/client/addClientLead.html", {"form": client_data, "title": "Add Project"})
    else:
        form = ClientDetailForm()
        return render(request, "new/client/addClientLead.html", {"form": form, "title": "Add Project"})


# this methode was showing , resource attendance list in client details
@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin", "client"], login_url="/unauthorized/")
def resource_worklog(request):
    get_user_role(request, request.user)
    project_id = request.POST.get('project_id')
    resource_id = request.POST.get('resource_id')
    resource_worklog_attendance = Worklog.objects.filter(project_id=project_id, resource_id=resource_id)
    html = render_to_string("new/client/resource_worklog.html", {"worklog_list": resource_worklog_attendance},
                            request=request)
    return JsonResponse({'worklog': html})


# view client Query List in Client Details
@user_passes_test(
    lambda user: get_user_role(request, user) in ["client", "resource_lead", 'project_manager', 'business_analyst',
                                                  'solution_architect', 'frontend_team_lead', 'backend_team_lead',
                                                  'resource_member'], login_url="/unauthorized/")
def clientQuery(request):
    user_role = get_user_role(request, request.user)
    if user_role == 'client':
        current_user_email = request.user.email_id
        instance = ClientDetail.objects.get(email_id=current_user_email)
        details = ProjectDetail.objects.filter(client=instance)
        query_list = RaisedQueryDetail.objects.filter(project__in=details)

    elif user_role == 'resource_lead':
        current_user_email = request.user.email_id
        resource = ResourceDetail.objects.filter(email_id=current_user_email)
        details = ResourceProjectDetail.objects.filter(resource__in=resource, role='Lead').values('project')
        query_list = RaisedQueryDetail.objects.filter(project__in=details)

    elif user_role == 'project_manager':
        current_user_email = request.user.email_id
        resource = ResourceDetail.objects.filter(email_id=current_user_email)
        details = ResourceProjectDetail.objects.filter(resource__in=resource).values('project')
        query_list = RaisedQueryDetail.objects.filter(project__in=details, resource__in=details)

    elif user_role == 'solution_architect':
        current_user_email = request.user.email_id
        resource = ResourceDetail.objects.filter(email_id=current_user_email)
        details = ResourceProjectDetail.objects.filter(resource__in=resource).values('project')
        query_list = RaisedQueryDetail.objects.filter(project__in=details, resource__in=details)

    elif user_role == 'frontend_team_lead':
        current_user_email = request.user.email_id
        resource = ResourceDetail.objects.filter(email_id=current_user_email)
        details = ResourceProjectDetail.objects.filter(resource__in=resource).values('project')
        query_list = RaisedQueryDetail.objects.filter(project__in=details, resource__in=details)

    elif user_role == 'backend_team_lead':
        current_user_email = request.user.email_id
        resource = ResourceDetail.objects.filter(email_id=current_user_email)
        details = ResourceProjectDetail.objects.filter(resource__in=resource).values('project')
        query_list = RaisedQueryDetail.objects.filter(project__in=details, resource__in=details)
    else:
        query_list = RaisedQueryDetail.objects.all()
    return render(request, "new/client/query_list.html", {"query_list": query_list, "title": "Client Query"})


# view Resource List in worklog on Client Details
@user_passes_test(lambda user: get_user_role(request, user) in ["client"], login_url="/unauthorized/")
def clientResourceList(request):
    current_user_email = request.user.email_id
    client = ClientDetail.objects.get(email_id=current_user_email)
    project_list = ProjectDetail.objects.filter(client=client)
    resource_client_list = ResourceProjectDetail.objects.filter(project__in=project_list)
    return render(request, "new/client/resource_list.html",
                  {"resource_list": resource_client_list, "title": "Resource List"})


# view Resource List on Resource Lead
def Lead_resourceList(request):
    current_user_email = request.user.email_id
    resource = ResourceDetail.objects.filter(email_id=current_user_email)
    resource_projects = ResourceProjectDetail.objects.filter(resource__in=resource)
    return render(request, "new/resource/lead_resourceList.html",
                  {"resource_list": resource_projects, "title": "Resource List"})


# view Client Project On View User
@user_passes_test(lambda user: get_user_role(request, user) in ["client"], login_url="/unauthorized/")
def clientProjects(request):
    """This view will show projects to the client"""
    current_user_email = request.user.email_id
    client = ClientDetail.objects.get(email_id=current_user_email)
    client_project = ProjectDetail.objects.filter(client=client)
    return render(request, "new/client/view_viewClient.html",
                  {"client_project": client_project, "title": "Client Project"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def viewClient(request):
    client_list = ClientDetail.objects.all()
    return render(request, "new/client/viewClient.html", {"client_list": client_list, "title": "Client List"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def alterClient(request, id):
    client = get_object_or_404(ClientDetail, id=id)
    client_form = ClientDetailForm(request.POST or None, instance=client)
    if request.method == "POST":
        if client_form.is_valid():
            client_form.save()
            messages.success(request, "Successfully Changed check bellow...! ")
            return redirect("view_client")
        else:
            messages.warning(request, "Invaid changes ! ")
            return redirect("view_client")
    return render(request, "new/client/update.html", {"form": client_form, "title": "Update Project"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def deleteClient(request, id):
    client = get_object_or_404(ClientDetail, pk=id)
    if client != None:
        ClientDetail.delete(client)
        messages.success(request, "Successfully deleted Client... !")
        return redirect("view_client")
    else:
        messages.warning(request, "Client data not found!")
        return redirect("view_client")


# ResourceProjectDetail
# ADD
@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def addResourceProject(request):
    if request.method == "POST":
        form = ResourceDetailForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully added!")
            return redirect("viewResource")
        else:
            print(form)
            return render(request, "new/resource/addResource.html",
                          context={"form": form, "title": "Add ResourceProject"})
    else:
        form = ResourceDetailForm()
        return render(request, "new/resource/addResource.html", context={"form": form, "title": "Add ResourceProject"})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def updateResourceProject(request, id):
    instance = get_object_or_404(ResourceDetail, id=id)
    form = ResourceDetailForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully Changed! ")
            return redirect("viewResource")
        else:
            return render(request, "new/resource/update.html", {"form": form, "title": "Update ResourceProject"})
    return render(request, "new/resource/update.html", {"form": form, "title": "Update ResourceProject"})


# DELETE
@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin"], login_url="/unauthorized/")
def deleteResourcePage(request, id):
    instance = get_object_or_404(ResourceDetail, id=id)
    if instance is not None:
        ResourceDetail.delete(instance)
        messages.success(request, "Successfully deleted!")
        return redirect("viewResource")
    else:
        messages.warning(request, "Details not found!")
        return redirect("viewResource")


# VIEW
# This View List Show Are Worklog List in Resource Member User, Resource Lead User
def viewList(request):
    """ This Method will return the worklog of the resource"""

    role = get_user_role(request, request.user)
    if role == 'project_manager':
        resource = ResourceDetail.objects.filter(email_id=request.user.email_id)
        devops = ResourceDetail.objects.filter(role='DevOps')
        quality_analyst = ResourceDetail.objects.filter(role='Quality Analyst')
        lead_team = ResourceDetail.objects.filter(role="Frontend Team Lead")
        backend_team_lead = ResourceDetail.objects.filter(role='Backend Team Lead')
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        all_worklog_list = Worklog.objects.filter(project__in=project,
                                                  resource__in=resource | devops | quality_analyst | lead_team | backend_team_lead)
    elif role == 'business_analyst':
        worklog = ResourceDetail.objects.filter(role__in=['Solution Architect', 'Business Analyst'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        all_worklog_list = Worklog.objects.filter(project__in=project, resource__in=worklog)
    elif role == 'solution_architect':
        worklog = ResourceDetail.objects.filter(
            role__in=['Solution Architect', 'Frontend Team Lead', 'Backend Team Lead'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        all_worklog_list = Worklog.objects.filter(project__in=project, resource__in=worklog)
    elif role == 'backend_team_lead' or role == 'frontend_team_lead':
        worklog = ResourceDetail.objects.filter(
            role__in=['Backend Team Lead', 'Frontend Senior Software Engineer', 'Junior Software Engineer',
                      'Frontend Software Engineer', 'Backend Software Engineer', 'Backend Senior Software Engineer'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        all_worklog_list = Worklog.objects.filter(project__in=project, resource__in=worklog)
    else:
        resource = ResourceDetail.objects.get(email_id=request.user.email_id)
        all_worklog_list = Worklog.objects.filter(resource=resource)
    return render(request, "new/worklog/viewList.html", {"worklog_list": all_worklog_list, "title": "Worklog List"})


def get_user_role(request, user):
    # current_user_email = get_email(request)
    try:
        current_user_email = user.email_id
    except:
        current_user_email = user.email
    try:
        user = ResourceDetail.objects.get(email_id=current_user_email)
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Project Manager').exists():
            return 'project_manager'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Lead Generator').exists():
            return 'lead_generator'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Frontend Team Lead').exists():
            return 'frontend_team_lead'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Business Analyst').exists():
            return 'business_analyst'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Solution Architect').exists():
            return 'solution_architect'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Business Development Execute').exists():
            return 'business_development_execute'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Backend Team Lead').exists():
            return 'backend_team_lead'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='HumanResource').exists():
            return 'human_resource'
        else:
            return "resource_member"
    except ResourceDetail.DoesNotExist:
        try:
            ClientDetail.objects.get(email_id=current_user_email)
            return 'client'
        except ClientDetail.DoesNotExist:
            try:
                User.objects.get(email=current_user_email)
                return 'superadmin'
            except User.DoesNotExist:
                return None


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin", "client", "project_manager",
                                                                'business_development_execute', 'business_analyst',
                                                                'solution_architect', 'backend_team_lead',
                                                                'frontend_team_lead', 'resource_member'],
                  login_url="/unauthorized/")
def resourceProjects(request):
    resource_current_email = request.user.email_id
    resource = ResourceDetail.objects.filter(email_id=resource_current_email).first()
    resource_projects = ResourceProjectDetail.objects.filter(resource=resource)
    return render(request, "new/resource/view_viewResource.html",
                  {"resource_projects": resource_projects, "title": "Resource Projects"})


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def viewResource(request):
    resources = ResourceDetail.objects.all()
    return render(request, "new/resource/viewResource.html", {"resources": resources, "title": "Resource List"})


# attend model
# @user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'project_manager', 'business_analyst',
#                                                                 'solution_architect', 'backend_team_lead',
#                                                                 'frontend_team_lead', 'business_development_execute'],
#                   login_url="/unauthorized/")
def addAttend(request):
    role = get_user_role(request, request.user)
    if request.method == "POST":

        resource_list = [int(item) for item in request.POST.getlist("resource")]
        resource_list = ResourceDetail.objects.filter(id__in=resource_list)
        project_name = ProjectDetail.objects.get(id=int(request.POST["project"]))
        for i in range(len(resource_list)):
            try:
                attend_object = Attend(date=request.POST["date"], resource=resource_list[i], project=project_name)
                attend_object.save()
                subject = "Attendence"
                try:
                    username = request.user.username
                except AttributeError:
                    first_name = request.user.first_name
                    last_name = request.user.last_name
                    username = first_name + " " + last_name
                context = {
                    "fullname": resource_list[i].first_name + " " + resource_list[i].last_name,
                    "project_name": project_name.name,
                    "added_by": username,
                    "date": request.POST["date"]
                }
                message = render_to_string("new/users/attendence_email.html", request=request, context=context)
                email = EmailMessage(subject, message, to=[resource_list[i].email_id])
                try:
                    email.send(fail_silently=False)
                except:
                    messages.success(request, f"Successfully added and email was not sent to {username}")
            except:
                messages.warning(request, "Attendance has been Already added")
                return redirect("resource_attend")

        messages.success(request, "Successfully added and email sent...")
        if role == 'resource_member':
            return redirect("resource_attend")
        else:
            return redirect("view_attend")
    else:
        eform = AttendForm(user=request.user)
        return render(request, "new/attend/addAttend.html", {'form': eform, "title": "Add Attend"})


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'project_manager', 'backend_team_lead',
                                                                'frontend_team_lead', 'business_development_execute',
                                                                'business_analyst', 'human_resource'],
                  login_url="/unauthorized/")
def viewAttend(request):
    role = get_user_role(request, request.user)

    if role in ['project_manager', "business_analyst"]:
        attendance = ResourceDetail.objects.filter(
            role__in=['Business Development Execute', 'Lead Generator', 'Quality Analyst', 'DevOps',
                      'Frontend Team Lead', 'Backend Team Lead'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        data = Attend.objects.filter(project__in=project, resource__in=attendance)

    elif role == 'solution_architect':
        attendance = ResourceDetail.objects.filter(
            role__in=['Frontend Team Lead', 'Backend Team Lead'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        data = Attend.objects.filter(project__in=project, resource__in=attendance)
    elif role == 'frontend_team_lead':
        attend = ResourceDetail.objects.filter(
            role__in=['Frontend Senior Software Engineer', 'Junior Software Engineer',
                      'Frontend Software Engineer', 'Backend Software Engineer', 'Backend Senior Software Engineer'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        data = Attend.objects.filter(project__in=project, resource__in=attend)
    elif role == 'backend_team_lead':
        attend = ResourceDetail.objects.filter(
            role__in=['Frontend Senior Software Engineer', 'Junior Software Engineer',
                      'Frontend Software Engineer', 'Backend Software Engineer', 'Backend Senior Software Engineer'])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        data = Attend.objects.filter(project__in=project, resource__in=attend)

    elif role == 'business_development_execute':
        resource = ResourceDetail.objects.filter(role__in=['Lead Generator'])
        bde_project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        data = Attend.objects.filter(resource__in=resource, project__in=bde_project)
    
    elif role == 'human_resource':
        role_in=['Business Analyst', 'Solution Architect', 'Backend Team Leadr', 'Frontend Team Lead', 'Business Development Execute', 'Project Manager']
        resource = ResourceDetail.objects.filter(role__in=role_in)
        data = Attend.objects.filter(resource__in=resource)

    else:
        data = Attend.objects.all()
    return render(request, "new/attend/viewAttend.html", {"data": data, "title": "View Attends"})


def get_attendance_status(request):
    """Get Attend Application using Json data"""
    try:
        attend_id = request.POST.get('attend_id')
        # print(attend_id)
        html = render_to_string("new/attend/updateAttendStatus.html", {'attend_id': attend_id, }, request=request)
        return JsonResponse({'update': html})
    except:
        return redirect('view_attends')


def update_attend_status(request):
    """Update attendance status for leaders"""
    try:
        if request.method == 'POST' and request.POST["action"] == "approve":
            attend_ids = request.POST.get('attend_ids').split(',')
            ids = []
            for i in attend_ids:
                try:
                    ids.append(int(i))
                except:
                    pass
            instance = Attend.objects.filter(id__in=ids)
            instance.update(status = "Approved")
            messages.success(request, "Attendance saved..!")
            return redirect('view_attend')
    except:
        pass
    
    if request.method == 'POST':
        attend_id = request.POST.get('attend_id_to_update')
        attend_status = request.POST.get('attend_status')
        instance = get_object_or_404(Attend, id=attend_id)
        instance.status = attend_status
        instance.save()
        messages.success(request, "Attendance saved..!")
        return redirect('view_attend')
    else:
        return redirect('view_attend')


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def updateAttend(request, id):
    data = get_object_or_404(Attend, id=id)
    form = AttendForm(request.POST or None, instance=data, user=request.user)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Attend Updated Successfully...")
            return redirect("view_attend")
        else:
            return render(request, "new/attend/updateAttend.html", {'form': form, "title": "Update Attend"})
    return render(request, "new/attend/updateAttend.html", {'form': form, "title": "Update Attend"})


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def deleteAttend(request, id):
    query = get_object_or_404(Attend, id=id)
    if query is not None:
        Attend.delete(query)
        messages.success(request, "Deleted Successfully Attend... !")
        return redirect('view_attend')
    else:
        messages.warning(request, "Undefiend Error...!")
        return redirect('index')


@user_passes_test(
    lambda user: get_user_role(request, user) in ['resource_member', 'project_manager', 'backend_team_lead',
                                                  'frontend_team_lead', 'business_development_execute',
                                                  'lead_generator'], login_url="/unauthorized/")
def resourceAttend(request):
    current_user_email = request.user.email_id
    resource = ResourceDetail.objects.filter(email_id=current_user_email).first()
    attendence = Attend.objects.filter(resource=resource)
    if ResourceProjectDetail.objects.filter(resource=request.user).exists():
        worklog = Worklog.objects.filter(resource=request.user)
        context = {"attendence": attendence,
                   "worklog": worklog,
                   "title": "Resource Attend"
                   }
        return render(request, "new/attend/resource_attend.html", context=context)
    return render(request, "new/attend/resource_attend.html", {"attendence": attendence, "title": "Resource Attend"})


# ResourceProjectDetail
# ADD DETAILS
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def addResourceProjectDetail(request):
    if request.method == "POST":
        form = ResourceProjectDetailForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully added!")
            return redirect("project_resource")
        else:
            messages.warning(request, "Incorrect details!")
            return redirect("addResourceProjectDetail")
    else:
        form = ResourceProjectDetailForm()
        return render(request, 'new/resourceProjectDetails/addDetails.html',
                      context={"form": form, "title": "Add ResourceProject"})


# UPDATE DETAILS
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def updateResourceProjectDetail(request, id):
    instance = get_object_or_404(ResourceProjectDetail, id=id)
    form = ResourceProjectDetailForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully Changed!")
            return redirect("viewResourceProjectDetail")
        else:
            return render(request, "new/resourceProjectDetails/updateDetails.html",
                          {"form": form, "title": "Update ResourceProject"})
    return render(request, "new/resourceProjectDetails/updateDetails.html",
                  {"form": form, "title": "Update ResourceProject"})


# VIEW DETAILS
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def viewResourceProjectDetail(request):
    details = ResourceProjectDetail.objects.all()
    return render(request, "new/resourceProjectDetails/viewDetails.html",
                  {"details": details, "title": "View ResourceProject"})


# DELETE DETAILS
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def deleteResourceProjectDetail(request, id):
    instance = get_object_or_404(ResourceProjectDetail, id=id)
    if instance is not None:
        ResourceProjectDetail.delete(instance)
        messages.success(request, "Successfully deleted!")
        return redirect("project_resource")
    else:
        messages.warning(request, "Project not found!")
        return redirect("project_resource")


# RaisedQueryDetail
# ADD RQD
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'client'], login_url="/unauthorized/")
def addRQD(request):
    if request.method == "POST":
        client_form = RQDForm(request.POST, request.FILES, user=request.user)
        if client_form.is_valid():
            client_form.save()
            messages.success(request, "Successfully added!")
            role = get_user_role(request, request.user)
            if role == "superadmin":
                return redirect("viewRQD")
            else:
                return redirect("query_list")
        else:
            messages.debug(request, "Incorrect details!")
            return render(request, 'new/RQD/addRQD.html', {"form": client_form, "title": "Add ResourceQuery"})
    else:
        form = RQDForm(user=request.user)
        return render(request, 'new/RQD/addRQD.html', context={"form": form, "title": "Add ResourceQuery"})


# VIEW RQD
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def viewRQD(request):
    query = RaisedQueryDetail.objects.all()
    return render(request, "new/RQD/viewRQD.html", {"query": query, "title": "View ResourceQuery"})


# UPDATE RQD
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def updateRQD(request, id):
    instance = get_object_or_404(RaisedQueryDetail, id=id)
    form = RQDForm(request.POST or None, request.FILES or None, instance=instance, user=request.user)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully Changed!")
            return redirect("viewRQD")
        else:
            return render(request, "new/RQD/updateRQD.html", {"form": form, "title": "Update ResourceQuery"})
    return render(request, "new/RQD/updateRQD.html", {"form": form, "title": "Update ResourceQuery"})


# DELETE RQD
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def deleteRQD(request, id):
    instance = get_object_or_404(RaisedQueryDetail, id=id)
    if instance is not None:
        RaisedQueryDetail.delete(instance)
        messages.success(request, "Successfully deleted!")
        return redirect("viewRQD")
    else:
        messages.warning(request, "Project not found!")
        return redirect("viewRQD")


# BLOG MODEL FUNCTION
# add blog
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def addBlog(request):
    if request.method == "POST":
        form = BlogForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Blog Added Successflly..!")
            return redirect("view_blog")
        else:
            return render(request, "new/blog/addBlog.html", {"form": form, "title": "Add Blog"})
    else:
        form = BlogForm()
        return render(request, "new/blog/addBlog.html", {"form": form, "title": "Add Blog"})


# view blog model data function
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def viewBlog(request):
    blogs = Blog.objects.all()
    return render(request, "new/blog/viewBlog.html", {"blogs": blogs, "title": "View Blogs"})


# delete blog function
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def deleteBlog(request, id):
    instance = get_object_or_404(Blog, id=id)
    if instance is not None:
        Blog.delete(instance)
        messages.success(request, "Successfully deleted!")
        return redirect("view_blog")
    else:
        messages.warning(request, "Blog not found!")
        return redirect("view_blog")


# update blog data
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def alterBlog(request, id):
    blog_data = get_object_or_404(Blog, id=id)
    form = BlogForm(request.POST or None, request.FILES or None, instance=blog_data)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Updated succesfully")
            return redirect("view_blog")
        else:
            return render(request, "new/blog/update.html", {"form": form, "title": "Update Blog"})
    return render(request, "new/blog/update.html", {"form": form, "title": "Update Blog"})


# CLASS BASED VIEWS FOR API'S
# This view represents our blog api

class blogView(viewsets.ModelViewSet):
    permission_classes = [TokenHasReadWriteScope]
    serializer_class = blogSerializer
    queryset = Blog.objects.all()

    def list(self, request):
        queryset = Blog.objects.all()
        serializer = blogSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk, formate=None):
        queryset = Blog.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = blogSerializer(user)
        return Response(serializer.data)

        # return Response(data="You Have Not Permission To Update")

    def create(self, request, *args, **kwargs):
        # permission_classes = [TokenHasReadWriteScope]
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(data="Added Successfully")

    def destroy(self, request, pk=None):
        return Response(data="Not permission to Delete")


# This view represents our job applicant api

class applicantView(viewsets.ModelViewSet):
    permission_classes = [TokenHasReadWriteScope]
    serializer_class = applicantSerializer
    queryset = JobApplicant.objects.all()

    def list(self, request):
        return Response(data="Not visible!")

    def retrieve(self, request, pk, formate=None):
        return Response(data="You Have Not Permission To Update")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(data="Added Successfully")

    def destroy(self, request, pk=None):
        return Response(data="Not permission to Delete")


# this view are represents all view Job Applicant
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def view_job_applicant(request):
    jobApplicant = JobApplicant.objects.all()
    return render(request, 'new/Apis View/jobApplicant.html', {'applicants': jobApplicant})


# This view represents our WebQueries api
class queryView(viewsets.ModelViewSet):
    permission_classes = [TokenHasReadWriteScope]
    serializer_class = querySerializer
    queryset = WebQueries.objects.all()

    def list(self, request):
        return Response(data="Not visible!")

    def retrieve(self, request, pk, formate=None):
        return Response(data="You Have Not Permission To Update")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(data="Added Successfully")

    def destroy(self, request, pk=None):
        return Response(data="Not permission to Delete")


# this view are represents all view Web Queries
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def view_web_queries(request):
    webQueries = WebQueries.objects.all()
    return render(request, 'new/Apis View/webQueries.html', {'applicants': webQueries, "title": "View WebQuery"})


# This View represents our subscriber
class subscriberView(viewsets.ModelViewSet):
    permission_classes = [TokenHasReadWriteScope]
    serializer_class = subscriberSerializer
    queryset = Subscriber.objects.all()
    """
       A simple ViewSet for listing or retrieving or Create or Delete for users.
    """

    def list(self, request):
        return Response(data="Not visible!")

    def retrieve(self, request, pk, formate=None):
        return Response(data="You Have Not Permission To Update")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(data="Added Successfully")

    def destroy(self, request, pk=None):
        return Response(data="Not permission to Delete")


# this view are represents all Subscriber Details
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def view_subscriber(request):
    subscriber = Subscriber.objects.all()
    return render(request, 'new/Apis View/view_subscriber.html',
                  {'applicants': subscriber, "title": "View Subscribers"})


# ADD WORKLOG
def addWorkLog(request):
    if request.method == "POST":
        res_form = WorklogForm(request.POST, user=request.user)
        if res_form.is_valid():
            res_form.save()
            today = datetime.date.today()
            if Attend.objects.filter(date=today, resource=request.user).exists():
                pass
            else:
                project = ProjectDetail.objects.get(pk=int(request.POST.get('project')))
                Attend.objects.create(date=today, resource=request.user, project=project)
            role = get_user_role(request, request.user)
            if role == "superadmin":
                return redirect("view_worklog")
            else:
                messages.success(request, "Worklog Added Successfully")
                return redirect("view_list")
        else:
            return render(request, "new/worklog/addworklog.html", {"form": res_form, "title": "Add Worklog"})
    else:
        sup_form = WorklogForm(user=request.user)
        return render(request, "new/worklog/addworklog.html", {"form": sup_form, "title": "Add Worklog"})


# VIEW WORKLOG
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'human_resource'], login_url="/unauthorized/")
def viewWorklog(request):
    worklog_list = Worklog.objects.all()
    return render(request, "new/worklog/viewWorklog.html", {"worklog": worklog_list, "title": "Worklog List"})


# UPDATE WORKLOG
def updateWorklog(request, id):
    instance = get_object_or_404(Worklog, id=id)
    form = WorklogForm(request.POST or None, instance=instance, user=request.user)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully Changed!")
            role = get_user_role(request, request.user)
            if role == "superadmin":
                return redirect("view_worklog")
            else:
                return redirect("view_list")
        else:
            return render(request, "new/worklog/updateworklog.html", {"form": form, "title": "Update Worklog"})
    return render(request, "new/worklog/updateworklog.html", {"form": form, "title": "Update Worklog"})


# Delete WORKLOG
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def deleteWorklog(request, id):
    instance = get_object_or_404(Worklog, id=id)
    if instance is not None:
        Worklog.delete(instance)
        messages.success(request, "Successfully deleted!")
        return redirect("view_worklog")
    else:
        messages.warning(request, "Blog not found!")
        return redirect("view_worklog")


# add Lead model
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'lead_generator', 'business_development_execute', 'business_analyst'],
                  login_url="/unauthorized/")
def addLead(request):
    user_type = get_user_role(request, request.user)
    if ResourceDetail.objects.filter(role="Lead Generator"):
        if request.method == "POST":
            form = LeadForm(request.POST, user=request.user)
            form.instance.added_by = request.user
            if form.is_valid():
                try:
                    form.save()
                    try:
                        if form.instance.added_by:
                            lead = LeadModel.objects.latest('created_at')
                            add_lead_notification(lead)
                    except:
                        messages.warning(request, "Notification hasn't been Sent..!")
                        pass
                    messages.success(request, "Successfully Lead...")
                    return redirect("view_lead")
                except:
                    messages.warning(request, "Lead Already Added...")
                    return redirect("add_lead")
            else:
                return render(request, "new/lead/addLead.html", {'form': form, "title": "Add Lead"})
        else:
            form = LeadForm(user=request.user)
            return render(request, "new/lead/addLead.html", {'form': form, "title": "Add Lead"})
    else:
        messages.warning(request, "You dont have permission to add Lead...!")
        return redirect('index')


@user_passes_test(
    lambda user: get_user_role(request, user) in ['superadmin', 'lead_generator', 'business_development_execute', 'business_analyst'],
    login_url="/unauthorized/")
def viewLead(request):
    type = get_user_role(request, request.user)
    data = None
    if type == 'superadmin':
        data = LeadModel.objects.all()
        return render(request, "new/lead/viewLeadSU.html", {"data": data})
    elif type == 'lead_generator':
        resource = ResourceDetail.objects.filter(email_id=request.user.email_id).first()
        data = LeadModel.objects.filter(added_by=resource)
    elif type in ['business_development_execute', 'business_analyst']:
        data = LeadModel.objects.all()
    else:
        return redirect('index')
    return render(request, "new/lead/viewLead.html", {"data": data, "title": "Lead List"})


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'lead_generator'],
                  login_url="/unauthorized/")
def updateLead(request, id):
    user_type = get_user_role(request, request.user)
    if ResourceDetail.objects.filter(role='Lead Generator') or LeadModel.objects.filter(pk=id, status="New").exists():
        data = get_object_or_404(LeadModel, id=id)
        form = LeadForm(request.POST or None, instance=data, user=request.user)
        if request.method == "POST":
            if form.is_valid():
                form.save()
                try:
                    if form.instance.added_by:
                        update_lead = LeadModel.objects.latest('updated_at')
                        id = LeadModel.objects.filter(pk=id)
                        update_lead_notification(update_lead, id)
                except:
                    messages.warning(request, "Notification hasn't been Sent..!")
                    pass
                messages.success(request, "Lead Updated Successfuly...")
                return redirect("view_lead")
            else:
                return render(request, "new/lead/update.html", {'form': form, "title": "Update Lead"})
        return render(request, "new/lead/update.html", {'form': form, "title": "Update Lead"})
    else:
        messages.warning(request, "You don't have permission to Update Lead...!")
        return redirect('view_lead')


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'resource'], login_url="/unauthorized/")
def deleteLead(request, id):
    user_type = get_user_role(request, request.user)
    if user_type == "resource" and not LeadModel.objects.filter(pk=id, status="New").exists():
        query = get_object_or_404(LeadModel, id=id)
        if query is not None:
            LeadModel.delete(query)
            messages.success(request, "Deleted Successfully Lead... !")
            return redirect('view_lead')
        else:
            messages.warning(request, "Undefined Error...!")
            return redirect('index')
    else:
        messages.warning(request, "You dont have permission to delete Lead...!")
        return redirect('view_lead')


# add Lead Follow Up model Functions
@user_passes_test(
    lambda user: get_user_role(request, user) in ['superadmin', 'business_development_execute', 'lead_generator'],
    login_url="/unauthorized/")
def addLeadFU(request):
    user_type = get_user_role(request, request.user)
    if user_type in ['business_development_execute', 'lead_generator']:
        if request.method == "GET":
            form = LeadFollwUpForm(lead_id=request.GET.get('lead_id'))
            if form.is_valid():
                form.save()
                messages.success(request, "Successfully added...")
                return redirect("view_lead_fu")
            html = render_to_string("new/lead-follow-up/addLeadFU.html", {"form": form}, request=request)
            return JsonResponse({'form': html})

        if request.method == "POST":
            form = LeadFollwUpForm(request.POST, request.FILES)
            form.instance.added_by = request.user
            if form.is_valid():
                form.save()
                messages.success(request, "Successfully added...")
                return redirect("view_lead")
            else:
                messages.warning(request, "Invalid Form ! Select Lead Option.. ")
                return redirect('view_lead')
    else:
        messages.warning(request, "Don't have permission to add Lead Follow up.. ")
        return redirect('view_lead_fu')


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'resource'], login_url="/unauthorized/")
# View for View Lead options To view FollowUp Details
def viewLeadFU(request):
    type = get_user_role(request, request.user)
    if request.is_ajax():
        lead_id = request.POST.get('lead_id')
        data = LeadFollowUP.objects.filter(lead=lead_id)
        html = render_to_string("new/lead-follow-up/viewLeadFUforSU.html", {"data": data}, request=request)
        return JsonResponse({'html': html})

    if type == 'superadmin':
        data = LeadFollowUP.objects.all()
        return render(request, "new/lead-follow-up/viewLeadFU.html",
                      {"data": data, "user": request.user, "title": "View LeadFU"})
    elif type == 'resource':
        resource = ResourceDetail.objects.filter(email_id=request.email_id).first()
        data = LeadFollowUP.objects.filter(added_by=resource)
    else:
        return redirect('index')
    return render(request, "new/lead-follow-up/viewLeadFU.html",
                  {"data": data, "user": request.user, "title": "View LeadFU"})


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'resource'], login_url="/unauthorized/")
def updateLeadFU(request, id):
    type = get_user_role(request, request.user)
    if type == 'resource':
        data = get_object_or_404(LeadFollowUP, id=id)
        form = LeadFollwUpForm(request.POST or None, request.FILES or None, instance=data)
        if request.method == "POST":
            if form.is_valid():
                form.save()
                messages.success(request, "Lead Follow up Updated Successfuly...")
                return redirect("view_lead_fu")
            else:
                return render(request, "new/lead-follow-up/update.html", {'form': form, "title": "Update LeadFU"})
        return render(request, "new/lead-follow-up/update.html", {'form': form, "title": "Update LeadFU"})
    else:
        messages.warning(request, "Only Resource can edit this...")
        return redirect('view_lead_fu')


@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'resource'], login_url="/unauthorized/")
def deleteLeadFU(request, id):
    type = get_user_role(request, request.user)
    if type == 'resource':
        query = get_object_or_404(LeadFollowUP, id=id)
        if query is not None:
            LeadFollowUP.delete(query)
            messages.success(request, "Deleted Successfully Lead... !")
            return redirect('view_lead_fu')
        else:
            messages.warning(request, "Undefined Error...!")
            return redirect('index')
    else:
        messages.warning(request, "Only Resource can delete this...")
        return redirect('view_lead_fu')


# view lead follow up of selected project
@user_passes_test(
    lambda user: get_user_role(request, user) in ['superadmin', 'lead_generator', 'business_development_execute'],
    login_url="/unauthorized/")
def viewFUofLead(request):
    if request.is_ajax():
        lead_id = request.POST.get('lead_id')
        lead_id = int(lead_id)
        data = LeadFollowUP.objects.filter(lead=lead_id)
        html = render_to_string('new/lead-follow-up/viewLeadFU-proj-lead.html', {'data': data}, request=request)
        return JsonResponse({'html': html})


# view RQD of Project
def viewRQDofProject(request):
    project_id = request.POST.get('project_id')
    # project_id = int(project_id)
    query = RaisedQueryDetail.objects.filter(project=project_id)
    html = render_to_string('new/project/projectQuery.html', {'query': query}, request=request)
    return JsonResponse({'html': html})


# view list of projects with RQD
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def viewProj_with_RQD(request):
    list_of_projects = ProjectDetail.objects.all()
    context = {
        "list_of_projects": list_of_projects,
        "title": "Project RQ List"
    }
    return render(request, 'new/project/viewprojectswithRQD.html', context=context)


def unAuthorized(request):
    """This View will redirect the un-authorized person
    to 401 page"""
    return render(request, "new/401.html", {"title": "Unauthorized"})


# Calendar View For Attendance, Leave, Event Are Showing
def calendarView(request):
    try:
        current_user_email = request.user.email_id
    except:
        current_user_email = request.user.email
    calendar_event = list()

    # This function are return all sunday and saturday in holiday
    sunday = datetime.datetime(2015, 12, 13)
    saturday = datetime.datetime(2022, 1, 29)
    for availability in range(1000):
        sunday = sunday + datetime.timedelta(days=7)
        saturday = saturday + datetime.timedelta(days=7)
        event_dict = {
            'id': 'att-' + str(availability),
            'name': 'Sunday',
            'date': str(sunday),
            'type': "holiday",
            'color': "red",
        }
        calendar_event.append(event_dict)
        event_dict1 = {
            'id': 'att-' + str(availability),
            'name': 'Company Holiday',
            'date': str(saturday),
            'type': "holiday",
            'color': "red",
        }
        calendar_event.append(event_dict1)

    # This Methode Return All Leave Days Show
    if ResourceDetail.objects.filter(email_id=current_user_email).exists():
        resource = ResourceDetail.objects.get(email_id=current_user_email)
        leaves = leaveModel.objects.filter(resource=resource)
        for leave in leaves:
            start_date = leave.start_date
            end_date = leave.end_date
            total_days = end_date - start_date
            for x in range(total_days.days):
                leave_range_dict = {
                    'id': 'wt-' + str(x) + str(leave.id),
                    'name': str(leave.reason),
                    'date': str(start_date + timedelta(days=x)),
                    'description': str(leave.status),
                    'type': "holiday",
                    'color': "#F4D03F",
                }
                calendar_event.append(leave_range_dict)

    # This Methode Return Event Leave Days Show
    leave_calendar = LeaveCalendar.objects.all()
    for x in leave_calendar:
        leave_calendar = x.event
        event_dict1 = {
            'id': 'at-' + str(x.id),
            'name': leave_calendar,
            'date': str(x.date),
            'type': "holiday",
            'color': "#e03c31"
        }
        calendar_event.append(event_dict1)

    # This Methode Return Present Days Show For Any Resources
    if ResourceDetail.objects.filter(email_id=current_user_email).exists():
        resource = ResourceDetail.objects.get(email_id=current_user_email)
        attendance = Attend.objects.filter(resource=resource, status="Approved")
        for availability in attendance:
            event_dict = {
                'id': 'att-' + str(availability.id),
                'name': 'Present',
                'date': str(availability.date),
                'description': "Project Name" + " -> " + str(availability.project),
                'type': "event",
                'color': "#7cfc00",
            }
            calendar_event.append(event_dict)

    """ This block will be excuted when superadmin will change calendar list """
    resource_list = ResourceDetail.objects.all()
    if request.method == "POST" and request.POST["action"] == "Show":
        resource_id = request.POST.get("resource_name")
        try:
            resource = resource_list.get(id=resource_id)
            resource_email = resource.email_id

            attendance = Attend.objects.filter(resource=resource, status="Approved")
            for availability in attendance:
                event_dict = {
                    'id': 'att-' + str(availability.id),
                    'name': 'Present',
                    'date': str(availability.date),
                    'description': "Project Name" + " -> " + str(availability.project),
                    'type': "event",
                    'color': "#7cfc00",
                }
                calendar_event.append(event_dict)
            return render(request, "new/attend/calendar.html",
                          {'calender_events': json.dumps(calendar_event), "resource_list": resource_list,
                           "resource_email": resource_email})
        except ResourceDetail.DoesNotExist:
            pass

    return render(request, "new/attend/calendar.html",
                  {'calender_events': json.dumps(calendar_event), "title": "Calender View",
                   "resource_list": resource_list})


# Show Resource Member Raised Query List
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin', 'resource_member'],
                  login_url="/unauthorized/")
def resourceRQD(request):
    return render(request, 'new/resource-query.html',
                  {'data': RaisedQueryDetail.objects.filter(resource=request.user), "title": "Resource RQ"})


def userProfile(request):
    """This method will show the user details of current user
    and will help to update them """

    user_email = get_email(request)
    try:
        user = ResourceDetail.objects.get(email_id=user_email)
        form = ResourceUpdateForm(request.POST or None, instance=user)
        if request.method == "POST":
            if form.is_valid():
                form.save()
                return redirect("index")
            else:
                return render(request, "new/users/user_profile.html", {"form": form, "title": "User Profile"})
    except ResourceDetail.DoesNotExist:
        try:
            user = ClientDetail.objects.get(email_id=user_email)
            form = ClientUpdateForm(request.POST or None, instance=user)
            if request.method == "POST":
                if form.is_valid():
                    form.save()
                    return redirect("index")
                else:
                    return render(request, "new/users/user_profile.html", {"form": form, "title": "User Profile"})
        except ClientDetail.DoesNotExist:
            form = None
    return render(request, "new/users/user_profile.html", {"form": form, "title": "User Profile"})


# Resource List of Project
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def resource_of_Project(request):
    query = ProjectDetail.objects.all()
    return render(request, 'new/project/resource-in-project.html', {"query": query, "title": "Resource Projects"})


# LEAVE CALENDAR
# Add Leave Calendar
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def add_leave_calendar(request):
    if request.method == "POST":
        form = LeaveCalendarForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully added Leave!")
            return redirect("view_leave_calendar")
        else:
            return render(request, "new/leave_calendar/addLeave.html", {'form': form, "title": "Add Leave"})
    else:
        form = LeaveCalendarForm()
        return render(request, "new/leave_calendar/addLeave.html", {'form': form, "title": "Add Leave"})


# View Leave Calendar
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def view_leave_calendar(request):
    view_leave = LeaveCalendar.objects.all()
    return render(request, "new/leave_calendar/viewLeave.html", {'view_leave': view_leave, "title": "View Leaves"})


# View leave Calendar For Any Resource
# @user_passes_test(lambda user: get_user_role(request,user) in ['superadmin'], login_url="/unauthorized/")
def resource_leave_calendar(request):
    resource_leave = LeaveCalendar.objects.all()
    return render(request, "new/leave_calendar/resource_leave_calendar.html",
                  {'view_leave': resource_leave, "title": "Resource Leaves"})


# Update Leave Calendar
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def update_leave_calendar(request, id):
    instance = get_object_or_404(LeaveCalendar, id=id)
    form = LeaveCalendarForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Successfully Changed!")
            return redirect("view_leave_calendar")
        else:
            return render(request, "new/leave_calendar/updateLeaves.html", {'form': form})
    return render(request, "new/leave_calendar/updateLeaves.html", {'form': form, "title": "Update ResourceLeave"})


# Delete Leave Calendar
@user_passes_test(lambda user: get_user_role(request, user) in ['superadmin'], login_url="/unauthorized/")
def delete_leave_calendar(request, id):
    instance = get_object_or_404(LeaveCalendar, id=id)
    if instance is not None:
        LeaveCalendar.delete(instance)
        messages.success(request, "Leave calendar Deleted..!")
        return redirect("view_leave_calendar")
    else:
        messages.warning(request, "Leave Calendar Not Found")
        return redirect("view_leave_calendar")


""" Views For Task Management"""


@csrf_exempt
def task_board(request):
    context = {}
    # task_form = TaskForm()
    role = get_user_role(request, request.user)

    """ This section will be excuted on the basis of user role """
    # START 
    if role == "superadmin":
        project = ProjectDetail.objects.filter(status="Active")
        list_of_boards = Board.objects.filter(project__in=project)
        # list_of_boards = Board.objects.all()
        try:
            board = Board.objects.all()[0]
            task_form_instance = Task(board=board)
            project_name = board.project.name
            board_name = board.name
            try:
                sprint = Sprint.objects.get(board=board, end_date__gte=datetime.date.today())
            except Sprint.DoesNotExist:
                sprint = None
            tasks = Task.objects.filter(board=board, sprint=sprint)
            pk_resources = ResourceProjectDetail.objects.filter(project=board.project).values('resource')
            list_of_resource = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(
                role__in=['Business Development Execute', 'Lead Generator', 'HumanResource'])
            resource_name = []
            if len(list_of_resource) > 0:
                for resource in list_of_resource:
                    resource_name.append({
                        'name': resource.first_name + ' ' + resource.last_name,
                        'initials': resource.first_name[0].upper() + resource.last_name[0].upper()
                    })

        except (IndexError, Task.DoesNotExist, Sprint.DoesNotExist) as e:
            list_of_boards = []
            tasks = []
            project_name = ""
            board_name = ""
            sprint = []
            list_of_resource = []
            resource_name = ""
    elif role == "client":
        try:
            project = ProjectDetail.objects.filter(client=request.user, status="Active")
            list_of_boards = Board.objects.filter(project__in=project)
            board = list_of_boards[0]
            project_name = board.project.name
            board_name = board.name
            try:
                sprint = Sprint.objects.get(board=board, end_date__gte=datetime.date.today())
            except Sprint.DoesNotExist:
                sprint = None
            tasks = Task.objects.filter(board=board, sprint=sprint)
            pk_resources = ResourceProjectDetail.objects.filter(project=board.project).values('resource')
            list_of_resource = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(
                role__in=['Business Development Execute', 'Lead Generator', 'HumanResource'])
            resource_name = []
            if len(list_of_resource) > 0:
                for resource in list_of_resource:
                    resource_name.append({
                        'name': resource.first_name + ' ' + resource.last_name,
                        'initials': resource.first_name[0].upper() + resource.last_name[0].upper()
                    })

        except (IndexError, Board.DoesNotExist, Task.DoesNotExist, Sprint.DoesNotExist) as e:
            list_of_boards = []
            tasks = []
            project_name = ""
            board_name = ""
            sprint = []
            list_of_resource = []
            resource_name = ""

    elif role in ["resource_member", "backend_team_lead", "frontend_team_lead", "business_analyst", "project_manager",
                  "lead_generator", "solution_architect", "business_development_execute"]:
        try:
            project = ProjectDetail.objects.filter(status="Active")
            list_of_projects = ResourceProjectDetail.objects.filter(resource=request.user, project__in=project).values("project")
            list_of_boards = Board.objects.filter(project__in=list_of_projects)
            board_name = list_of_boards[0].name
            project_name = list_of_boards[0].project.name
            try:
                sprint = Sprint.objects.get(board=list_of_boards[0], end_date__gte=datetime.date.today())
            except Sprint.DoesNotExist:
                sprint = None

            tasks = Task.objects.filter(board=list_of_boards[0], sprint=sprint)
            pk_resources = ResourceProjectDetail.objects.filter(project=list_of_boards[0].project).values('resource')
            list_of_resource = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(
                role__in=['Business Development Execute', 'Lead Generator', 'HumanResource'])
            resource_name = []
            if len(list_of_resource) > 0:
                for resource in list_of_resource:
                    resource_name.append({
                        'name': resource.first_name + ' ' + resource.last_name,
                        'initials': resource.first_name[0].upper() + resource.last_name[0].upper()
                    })

        except (IndexError, ResourceBoard.DoesNotExist, Task.DoesNotExist, Sprint.DoesNotExist) as e:
            list_of_boards = []
            tasks = []
            project_name = ""
            board_name = ""
            sprint = []
            list_of_resource = []
            resource_name = ""

    try:
        task_form_instance = Task(board=board, sprint=sprint)
        task_form = TaskForm(instance=task_form_instance, board=board)
    except:

        try:
            task_form_instance = Task(board=list_of_boards[0], sprint=sprint)
            task_form = TaskForm(instance=task_form_instance, board=list_of_boards[0])
        except:
            task_form_instance = None
            task_form = TaskForm()
        pass

    # END

    """ The below dict will carry data from backend to frontend """

    context["list_of_boards"] = list_of_boards
    context["section_list"] = ["ToDo", "In-Progress", "In-Review", "QA", "Re-Work", "Done"]

    context["form"] = task_form
    context["tasks"] = tasks
    context["project_name"] = project_name
    context["board_name"] = board_name
    context["sprint"] = sprint
    context["list_of_resource"] = list_of_resource
    context["resource_name"] = resource_name
    context["can_run_resource_progress"] = can_run_resource_progress(sprint)
    context["current_user"] = request.user

    if request.is_ajax() and "title" in request.POST:

        actual_task = set_slug_task(request.POST)
        form = TaskForm(actual_task)
        print(form.instance.start_date, form.instance.end_date, datetime.date.today())
        if form.is_valid():
            form.save()
            if form.instance.start_date == form.instance.end_date == datetime.date.today():
                task = Task.objects.latest("created_at")
                add_task_notification_id(form.instance.reporting, task)
            elif form.instance.start_date == datetime.date.today():
                task = Task.objects.latest("created_at")
                add_task_notification_id(form.instance.reporting, task)
            elif form.instance.end_date == datetime.date.today():
                task = Task.objects.latest("created_at")
                add_task_notification_id(form.instance.reporting, task)
            else:
                messages.warning(request, "Notification hasn't been sent..!")
                pass

            board = request.POST.get("board")
            try:
                sprint = Sprint.objects.get(board=board, end_date__gte=datetime.date.today())
            except Sprint.DoesNotExist:
                sprint = None
            tasks = Task.objects.filter(board=board, sprint=sprint)
            context["tasks"] = tasks
            html = render_to_string("new/task_management/ajax.html", request=request, context=context)
            return JsonResponse({"data": html})
        else:
            print("failed")

    if request.is_ajax() and request.POST["action"] == "view_task":
        task_id = request.POST["id"]
        task = Task.objects.get(id=task_id)
        comment = TaskComment.objects.filter(task=task)
        pk_resources = ResourceProjectDetail.objects.filter(project=task.board.project).values('resource')
        resources = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(
            role__in=['Business Development Execute', 'Lead Generator'])
        context = {
            "task": task,
            "comments": comment,
            "resource": resources,
            "current_user": request.user
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({"task": html})

    if request.is_ajax() and request.POST["action"] == "delete_task":
        task_id = request.POST["delete_task_id"]
        task = Task.objects.get(id=int(task_id))
        task.delete()
        board = Board.objects.get(name=request.POST["board_name"])
        try:
            sprint = Sprint.objects.get(board=board, end_date__gte=datetime.date.today())
        except Sprint.DoesNotExist:
            sprint = None
        tasks = Task.objects.filter(board=board, sprint=sprint)
        context["tasks"] = tasks
        html = render_to_string("new/task_management/ajax.html", request=request, context=context)
        return JsonResponse({"data": html})
    # this code for update title on view task popup
    if request.is_ajax() and request.POST["action"] == "update_title":
        task_id = request.POST["id"]
        task = Task.objects.get(id=task_id)
        task_title = request.POST["task_title"]
        task.title = task_title
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({"task": html})

    # this code for update description on view task popup
    elif request.is_ajax() and request.POST["action"] == "update_description":
        task_id = request.POST["id"]
        task = Task.objects.get(id=task_id)
        task_description = request.POST["task_description"]
        task.description = task_description
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task_form.instance.description, task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({"task": html})

    # this code for update status on view task popup
    elif request.is_ajax() and request.POST["action"] == "update_status":
        task_id = request.POST["id"]
        task = Task.objects.get(id=task_id)
        task_status = request.POST["update_status"]
        task.status = task_status
        task.save()
        try:
            if task_form.instance.status:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({"task": html})

    # this code for update assigned on view task popup
    elif request.is_ajax() and request.POST["action"] == "update_assigned":
        task_id = request.POST['id']
        task = Task.objects.get(id=task_id)
        task_assigned = ResourceDetail.objects.get(pk=request.POST["update_assigned"])
        task.assigned = task_assigned
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task,
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({"task": html})

    # this code for update labels on view task popup
    elif request.is_ajax() and request.POST["action"] == "update_labels":
        task_id = request.POST["id"]
        task = Task.objects.get(id=task_id)
        task_labels = request.POST["update_labels"]
        task.label = task_labels
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({"task": html})

    # this code for update start_date on view task popup
    elif request.is_ajax() and request.POST["action"] == 'update_start_date':
        task_id = request.POST['id']
        task = Task.objects.get(id=task_id)
        task_start = request.POST['start_date']
        task.start_date = task_start
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({'task': html})

    # this code for update end_date on view task popup
    elif request.is_ajax() and request.POST["action"] == 'update_end_date':
        task_id = request.POST['id']
        task = Task.objects.get(id=task_id)
        task_end = request.POST['end_date']
        task.end_date = task_end
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({'task': html})

    elif request.is_ajax() and request.POST["action"] == 'update_story_hour':
        task_id = request.POST['id']
        task = Task.objects.get(id=task_id)
        task_story_hour = request.POST['update_story_hour']
        task.story_hour = task_story_hour
        task.save()
        try:
            if task_form.instance:
                task = Task.objects.latest("updated_at")
                update_task_notification(task)
        except:
            messages.warning(request, "Notification hasn't been sent..!")
            pass
        context = {
            "task": task
        }
        html = render_to_string("new/task_management/task_view.html", request=request, context=context)
        return JsonResponse({'task': html})
    # END

    # Add Comment on task
    if request.is_ajax() and request.POST["action"] == "add_commit":

        task_id = request.POST["id"]
        task = Task.objects.get(id=task_id)

        if get_user_role(request, request.user) == "superadmin":
            print("Super user can't add comment")

        else:
            task_comment = request.POST["comment"]
            instance = TaskComment.objects.create(task=task, resource=request.user, comment=task_comment)
            instance.save()

        instance = TaskComment.objects.filter(task=task)
        context = {
            "task_comment": instance,
            "task": task,
            "current_user": request.user
        }
        html = render_to_string("new/task_management/task_comment.html", request=request, context=context)
        return JsonResponse({"task": html})

    # Edit and delete task Comment
    if request.is_ajax() and request.POST["action"] == "change_commit":

        task_id = request.POST["task_id"]
        comment_id = request.POST["comment_id"]
        what_to_do = request.POST['what_to_do']

        task = Task.objects.get(id=task_id)
        instance = TaskComment.objects.get(id=comment_id)

        if what_to_do == "delete":
            print(instance, "Deleted")
            instance.delete()

        elif what_to_do == "edit":
            task_comment = request.POST["comment"]
            instance.comment = task_comment
            instance.save()
        else:
            pass

        instance = TaskComment.objects.filter(task=task)
        context = {
            "task_comment": instance,
            "task": task,
            "current_user": request.user

        }
        html = render_to_string("new/task_management/task_comment.html", request=request, context=context)
        return JsonResponse({"task": html})

    if request.method == "POST" and request.POST["action"] == "Show":

        """ This block will be excuted when user will change board """

        board_name = request.POST.get("board_name")
        try:
            board = Board.objects.get(name=board_name)
            try:
                sprint = Sprint.objects.get(board=board, end_date__gte=datetime.date.today())
                task_form_instance = Task(board=board, sprint=sprint)
            except Sprint.DoesNotExist:
                sprint = None
                task_form_instance = None

            tasks = Task.objects.filter(board=board, sprint=sprint)
            try:
                sprint = Sprint.objects.get(board=board, end_date__gte=datetime.date.today())
            except Sprint.DoesNotExist:
                sprint = None

            try:
                task_form = TaskForm(instance=task_form_instance, board=board)
            except:
                task_form = TaskForm(board=board)

            context["tasks"] = tasks
            context["board_name"] = board_name
            context["project_name"] = board.project.name
            context["sprint"] = sprint
            context["form"] = task_form
            context["can_run_resource_progress"] = can_run_resource_progress(sprint)

            # This code for map resource list
            pk_resources = ResourceProjectDetail.objects.filter(project=board.project).values('resource')
            list_of_resource = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(
                role__in=['Business Development Execute', 'Lead Generator'])
            resource_name = []
            if len(list_of_resource) > 0:
                for resource in list_of_resource:
                    resource_name.append({
                        'name': resource.first_name + ' ' + resource.last_name,
                        'initials': resource.first_name[0].upper() + resource.last_name[0].upper()
                    })
            context["resource_name"] = resource_name
            return render(request, "new/task_management/board.html", context)
        except Board.DoesNotExist:
            pass

    return render(request, "new/task_management/board.html", context)


""" These views will be used to reset and set password """


def reset_password(request):
    if request.user.is_authenticated:
        return redirect("index")
    context = {}
    if request.method == "POST":
        email = request.POST["email"]
        try:
            user = ResourceDetail.objects.get(email_id=email)
        except ResourceDetail.DoesNotExist:
            try:
                user = ClientDetail.objects.get(email_id=email)
            except ClientDetail.DoesNotExist:
                context["message"] = "Email Does Not Exist"
                return render(request, "new/users/reset_password.html", context)
        context["fullname"] = user.first_name + " " + user.last_name
        context["domain"] = get_current_site(request).domain
        context["uidb64"] = urlsafe_base64_encode(force_bytes(user.email_id))
        context["token"] = PasswordResetTokenGenerator().make_token(user)
        message = render_to_string("new/users/password_email.html", request=request, context=context)
        subject = "Reset Password"
        email = EmailMessage(subject, message, to=[email])
        try:

            email.send(fail_silently=False)
        except:
            pass
        return render(request, "new/users/reset_message.html")
    else:
        context["message"] = ""
        context["title"] = "Reset Password"
        return render(request, "new/users/reset_password.html", context=context)


'''this method is used to view task through url'''


def view_task(request, slug):
    task = Task.objects.get(slug_task=slug)
    comment = TaskComment.objects.filter(task=task)
    pk_resources = ResourceProjectDetail.objects.filter(project=task.board.project).values('resource')
    resources = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(
        role__in=['Business Development Execute', 'Lead Generator'])
    context = {
        "task": task,
        "comments": comment,
        "resource": resources,
    }
    return render(request, "new/task_management/task_view_on_page.html", context)


def set_password(request, uidb64, token):
    """ This view is used to set new password for resource or client
    """
    email = urlsafe_base64_decode(force_text(uidb64))
    email = email.decode("utf-8")
    try:
        user = ResourceDetail.objects.get(email_id=email)
        form = ResourcePasswordReset()
    except ResourceDetail.DoesNotExist:
        try:
            user = ClientDetail.objects.get(email_id=email)
            form = ClientPasswordReset()
        except ClientDetail.DoesNotExist:
            return redirect("login")
    value = PasswordResetTokenGenerator().check_token(user, token)
    if value:
        if request.method == "POST":
            if isinstance(user, ResourceDetail):
                form = ResourcePasswordReset(request.POST, instance=user)
            else:
                form = ClientPasswordReset(request.POST, instance=user)
            if form.is_valid():
                form.save()
                return redirect("login")
            else:
                return render(request, "new/users/set_password.html", context={"form": form})
        else:
            return render(request, "new/users/set_password.html", context={"form": form})
    else:
        messages.warning(request, "Reset password link is invaild! ")
        return redirect("login")


def get_today_resource_leave(user):
    """this function return today leave of geted resouce in True or False"""
    today = datetime.date.today()

    try:
        leave_instance = leaveModel.objects.filter(resource=user, status="Approved")
        for leaves in leave_instance:
            if leaves.end_date == today:
                return False
            elif leaves.end_date > today:
                start_date = leaves.start_date
                end_date = leaves.end_date
                total_days = end_date - start_date
                total_days = (total_days.days) + 1
                for day in range(total_days + 1):
                    selected_date = start_date + timedelta(days=day)

                    if selected_date == today:
                        return True
            else:
                pass

        return False

    except:
        return False


def get_attend_status(user):
    """This method is used to get resource is - Present, Absent, Leave and ...."""

    attend_queryset = Attend.objects.filter(resource=user, date=datetime.date.today())

    if attend_queryset:
        return "Present"
    elif get_today_resource_leave(user):
        return "Leave"
    else:
        now = datetime.datetime.now()
        today6pm = now.replace(hour=18, minute=0, second=0, microsecond=0)
        if now < today6pm:
            return "Not Marked"
        return "Absent"


def set_slug_task(data):
    data._mutable = True
    board = Board.objects.get(id=int(data["board"]))
    total_tasks = Task.objects.filter(board=board).count()
    board_name = board.name.split(" ")
    slug_name = board_name[0][0] + board_name[1][0] if len(board_name) > 1 else board_name[0][0] + board_name[0][1]
    slug_name = slug_name.upper() + "-" + str(total_tasks)
    data["slug_task"] = slug_name
    data._mutable = False
    return data


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin", "project_manager", "business_analyst"],
                  login_url="/unauthorized/")
def add_sprint(request):
    # Add sprint method used in task board 

    today = datetime.date.today()
    if request.is_ajax():
        board_id = request.POST.get('board_id')
        board = Board.objects.get(pk=board_id)
        try:
            sprint = Sprint.objects.filter(board=board)[0]
            pre_end_date = sprint.end_date
            html = render_to_string("new/task_management/add_sprint.html", {'board': board}, request=request)
            return JsonResponse({'html': html, "permission": today <= pre_end_date, "date": pre_end_date})
        except (Sprint.DoesNotExist, IndexError):
            html = render_to_string("new/task_management/add_sprint.html", {'board': board}, request=request)
            return JsonResponse({'html': html, "permission": False, "date": today})

    if request.method == "POST":
        board_id = request.POST.get('board')
        name = request.POST.get('name')
        start_date = request.POST.get('start_date')
        end_date = request.POST.get('end_date')
        board = Board.objects.get(pk=int(board_id))
        try:
            previous_sprint = Sprint.objects.filter(board=board)[0]
        except:
            pass

        if board and name != "" and start_date != "" and end_date != "":
            if start_date > end_date:
                messages.warning(request, "Form Invalid..!")
                return redirect("task_board")
            Sprint.objects.create(board=board, name=name, start_date=start_date, end_date=end_date)
            new_sprint = Sprint.objects.filter(board=board)[0]
            try:
                previous_tasks = Task.objects.filter(sprint=previous_sprint).exclude(status="Done")
                for task in previous_tasks:
                    task.sprint = new_sprint
                    task.save()
            except:
                pass

            messages.success(request, "Sprint Added Successflly..!")
            return redirect("task_board")
        else:
            messages.warning(request, "Sprint Form Invalid..!")
            return redirect("task_board")
    else:
        return redirect("task_board")


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin", "project_manager", "business_analyst"],
                  login_url="/unauthorized/")
def resource_progress(request, id):
    """function to get resource progress on the basis of task completed"""
    sprint_id = id
    try:
        sprint = Sprint.objects.get(pk=sprint_id, end_date=datetime.date.today())
        project = Board.objects.get(id=sprint.board_id).project
        project_resource = ResourceProjectDetail.objects.filter(project=project)
        # tasks_with_unique_resource = Task.objects.filter(sprint=sprint).distinct('assigned')
        for res in project_resource:

            res_all_task = Task.objects.filter(sprint=sprint, assigned=res.resource).count()
            res_completeed_task = Task.objects.filter(sprint=sprint, status="Done", assigned=res.resource).count()

            complete_task_percent = int((res_completeed_task / res_all_task) * 100) if res_all_task else 0

            if complete_task_percent >= 90:
                grade = "OUTSTANDING"
            elif complete_task_percent >= 80:
                grade = "EXCELLENT"
            elif complete_task_percent >= 70:
                grade = "GOOD"
            elif complete_task_percent >= 50:
                grade = "AVERAGE"
            else:
                grade = "BELOW AVERAGE"

            query = ResourceProgress.objects.create(resource=res.resource, sprint=sprint, grade=grade,
                                                    percentage=complete_task_percent)
        messages.success(request, "Resource progress added successfully...!")

    except (Sprint.DoesNotExist, ZeroDivisionError):
        messages.warning(request, "Resource progress can't added...!")
    return redirect('task_board')


def get_progress(request):
    # this function return the Progress list of Resource (current user)
    user = request.user
    try:
        progress = ResourceProgress.objects.filter(resource=user)
    except:
        progress = None
    return progress


def can_run_resource_progress(sprint):
    """this function take a Sprint argument and check that
        Resource Progress have Created today for this Sprint or not
        if created will return False """
    try:
        today = datetime.date.today()
        '''after created resource progres of sprint in same day of board should return False'''
        try:
            res_progress = ResourceProgress.objects.filter(sprint=sprint)
            creatrd_at = [rp.created_at for rp in res_progress]
            if creatrd_at[0].date() == today:
                return False
        except:
            return sprint.end_date == today
    except:
        return False


"""return dict - resource name and his attendance count"""


def resources_attendance_count(resource_list):
    attend_count = {}
    for res in resource_list:
        name = res.resource.first_name + " " + res.resource.last_name
        if name in attend_count:
            attend_count[name] += 1
        else:
            attend_count[name] = 1
    return attend_count


def pre_month_res_attend_count(resource):
    """this inner function count resource attendance"""
    today = datetime.date.today()
    pre_month = today.month - 1
    if pre_month == 0:
        attend_count = Attend.objects.filter(date__month=12, date__year=today.year - 1, resource=resource).count()
    else:
        attend_count = Attend.objects.filter(date__month=pre_month, date__year=today.year, resource=resource).count()
    return attend_count


# team lead attandance , return dict, url = lead_self_attend
def lead_self_attend(request):
    today = datetime.date.today()
    previous_month = today.month - 1 if today.month - 1 != 0 else 12
    pre_month = today.month - 1
    su_dict = {}
    jan_2022 = datetime.date(2022, 1, 1)
    years = today.year - jan_2022.year
    month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    month_name = ["Jan", "Fab", "March", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    for x in range(years + 1):
        if jan_2022.year + x == today.year:
            months = []
            for m in range(1, today.month):
                months.append(m)
            su_dict[jan_2022.year + x] = month_name[:len(months)]
        else:
            su_dict[jan_2022.year + x] = month_name[:len(month_list)]

    try:
        if pre_month == 0:
            resource_list = Attend.objects.filter(date__month=12, date__year=today.year - 1, resource=request.user)
        else:
            resource_list = Attend.objects.filter(date__month=pre_month, date__year=today.year, resource=request.user)
        attend_count = resources_attendance_count(resource_list)
    except:
        attend_count = pre_month_res_attend_count(request.user)

    return render(request, "new/attend/pre_month_attendance.html", {"resource_attend": attend_count, "su_dict": su_dict,
                                                                    "selected_month": month_name[previous_month - 1]})


# Url total_attend_count
def resource_attend_count(request):
    """for super user only
    return last month attendance count list of resources"""
    role = get_user_role(request, request.user)
    today = datetime.date.today()
    if today.month - 1 == 0:
        selected_year = today.year - 1
        previous_month = 12
    else:
        selected_year = today.year
        previous_month = today.month - 1
    su_dict = {}
    jan_2022 = datetime.date(2022, 1, 1)
    years = today.year - jan_2022.year
    month_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
    month_name = ["Jan", "Fab", "March", "April", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

    for x in range(years + 1):
        if jan_2022.year + x == today.year:
            months = []
            for m in range(1, today.month):
                months.append(m)
            su_dict[jan_2022.year + x] = month_name[:len(months)]
        else:
            su_dict[jan_2022.year + x] = month_name[:len(month_list)]

    if request.is_ajax() and request.GET.get('action') == "change_month":
        year = request.GET.get('year')
        html = render_to_string("new/attend/month_list.html", {"data": su_dict[int(year)]}, request=request)

        return JsonResponse({'html': html})

    elif request.is_ajax():
        month = request.GET.get('month')
        year = request.GET.get('year')

        if role in ['superadmin', 'human_resource']:
            resource_list = Attend.objects.filter(date__month=month, date__year=year)

        elif role in ['project_manager', 'business_analyst']:
            resource = ResourceDetail.objects.filter(
                role__in=['Frontend Team Lead', 'DevOps', 'Quality Analyst', 'Backend Team Lead', ])
            pm_project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
            resource_lists = ResourceProjectDetail.objects.filter(resource__in=resource, project__in=pm_project).values(
                'resource')
            resource_list = Attend.objects.filter(date__month=month, date__year=year, resource__in=resource_lists)

        elif role in ["frontend_team_lead", "backend_team_lead"]:
            resource = ResourceDetail.objects.filter(
                role__in=["Junior Software Engineer", "Frontend Software Engineer", "Frontend Senior Software Engineer",
                          "Backend Software Engineer", "Backend Senior Software Engineer"])
            project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
            resource_lists = ResourceProjectDetail.objects.filter(resource__in=resource, project__in=project).values(
                'resource')
            resource_list = Attend.objects.filter(date__month=month, date__year=year, resource__in=resource_lists)

        else:
            resource_list = Attend.objects.filter(date__month=month, date__year=year, resource=request.user)

        if "total_resource_attend" in request.GET.get("url"):
            attend_count = resources_attendance_count(resource_list)
        else:
            attend_count = resources_attendance_count(
                Attend.objects.filter(date__month=month, date__year=year, resource=request.user))

        html = render_to_string("new/attend/pre_month_attend_ajax.html",
                                {"data": attend_count, "selected_month": month_name[int(month) - 1]}, request=request)
        return JsonResponse({'html': html})

    """return last month attendance count of resource"""
    role = get_user_role(request, request.user)

    if role in ["superadmin", "human_resource"]:
        attend_count = {}
        resource_list = Attend.objects.filter(date__month=previous_month, date__year=selected_year)
        attend_count = resources_attendance_count(resource_list)

    elif role in ['project_manager', 'business_analyst']:
        resource = ResourceDetail.objects.filter(
            role__in=['Frontend Team Lead', 'DevOps', 'Quality Analyst', 'Backend Team Lead'])
        pm_project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        resource_lists = ResourceProjectDetail.objects.filter(resource__in=resource, project__in=pm_project).values(
            'resource')
        resource_list = Attend.objects.filter(date__month=previous_month, date__year=selected_year,
                                              resource__in=resource_lists)
        attend_count = resources_attendance_count(resource_list)

    elif role in ["frontend_team_lead", "backend_team_lead"]:
        resource = ResourceDetail.objects.filter(
            role__in=["Junior Software Engineer", "Frontend Software Engineer", "Frontend Senior Software Engineer",
                      "Backend Software Engineer", "Backend Senior Software Engineer"])
        project = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        resource_lists = ResourceProjectDetail.objects.filter(resource__in=resource, project__in=project).values(
            'resource')
        resource_list = Attend.objects.filter(date__month=previous_month, date__year=selected_year,
                                              resource__in=resource_lists)
        attend_count = resources_attendance_count(resource_list)

    else:
        resource = Attend.objects.filter(date__month=previous_month, date__year=selected_year, resource=request.user)
        attend_count = resources_attendance_count(resource)

    return render(request, "new/attend/pre_month_attendance.html", {"resource_attend": attend_count, "su_dict": su_dict,
                                                                    "selected_month": month_name[previous_month - 1]})


@user_passes_test(lambda user: get_user_role(request, user) in ["superadmin", "frontend_team_lead", "backend_team_lead",
                                                                "project_manager", "business_analyst"],
                  login_url="/unauthorized/")
def resource_progress_list(request):
    role = get_user_role(request, request.user)
    if role == "superadmin":
        resource_progress = ResourceProgress.objects.all()
    elif role in ["frontend_team_lead", "backend_team_lead"]:
        resource_role_list = ["Junior Software Engineer", "Frontend Software Engineer",
                              "Frontend Senior Software Engineer", "Backend Software Engineer",
                              "Backend Senior Software Engineer"]
        projects = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        resources = ResourceDetail.objects.filter(role__in=resource_role_list)
        resource_list = ResourceProjectDetail.objects.filter(project__in=projects, resource__in=resources).values(
            "resource")
        resource_progress = ResourceProgress.objects.filter(resource__in=resource_list)
    elif role in ["project_manager", "business_analyst"]:
        projects = ResourceProjectDetail.objects.filter(resource=request.user).values('project')
        resource_list = ResourceProjectDetail.objects.filter(project__in=projects).values("resource")
        resource_progress = ResourceProgress.objects.filter(resource__in=resource_list)
    else:
        resource_progress = None
        messages.warning(request, "Thats unautherised...")
        return redirect('index')

    return render(request, "new/attend/resource_progress_view.html", {"data": resource_progress})


# This function is showing list of today not fill worklog
def worklog_list_not_add(request):
    role = get_user_role(request, request.user)
    if role == "superadmin":
        resources = ResourceDetail.objects.all()
        resource_list = []
        [resource_list.append(resource.id) for resource in resources]
        worklog = Worklog.objects.filter(date=datetime.date.today())
        worklog_resource_list = []
        [worklog_resource_list.append(work.resource.id) for work in worklog]
        worklog_not_added = []
        for resource in resource_list:
            if resource in worklog_resource_list:
                pass
            else:
                worklog_not_added.append(resource)
        resource_not_add_worklog = ResourceDetail.objects.filter(id__in=worklog_not_added)
        resource_not_add_worklog_list = []
        [resource_not_add_worklog_list.append(x.email_id) for x in resource_not_add_worklog]
        subject = "WorklogList"
        context = {
            "worklog_list": resource_not_add_worklog_list
        }
        message = render_to_string("new/users/worklog_email.html", request=request, context=context)
        email = EmailMessage(subject, message, 'info@wooshelf.com', ['dharmendra@wooshelf.com'])
        try:
            email.send()
            messages.success(request, "Email send..!")
        except:
            messages.warning(request, "Email was not sent to")
        return redirect("index")
    else:
        messages.warning(request, "email was not send")
        pass

