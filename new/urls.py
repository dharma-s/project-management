from django.contrib import admin
from django.urls import path, include, re_path
from new import views as new_view
from new import lead_follow_up_update
from new import leave_model_view
from django.conf import settings
from django.contrib.auth.views import LogoutView
from rest_framework import routers

"""Used to build api urls"""

router = routers.DefaultRouter()  # showing default router page
router.register("Blog", new_view.blogView, basename="Blog")
router.register("JobApplicant", new_view.applicantView, basename="JobApplicant")
router.register("WebQueries", new_view.queryView, basename="WebQueries")
router.register("Subscriber", new_view.subscriberView, basename="Subscriber")


urlpatterns = [
        
    re_path(r"^$", new_view.user_login, name="login"),
    re_path(r'^dashboard/$', new_view.index, name="index"),
    re_path(r"^logout/$", LogoutView.as_view(), {"next_page": settings.LOGOUT_REDIRECT_URL}, name="logout"),
    # CRUD OPERATIONS ON ProjectDetail Model
    re_path(r"^add_project/$", new_view.addProject, name="add_project"),
    re_path(r"^view_projects/$", new_view.viewProjects, name="view_projects"),
    re_path(r"^view_projects/update/(?P<id>\d+)/$", new_view.alterProject, name="alter_project"),
    re_path(r"^view_projects/delete/(?P<id>\d+)/$", new_view.deleteProject, name="delete_project"),
    
    # List of Project with RQD
    re_path(r"^view_project_query/$", new_view.viewRQDofProject, name="view_project_query"),

    # List of Project Resource
    re_path(r"^project_resource/$", new_view.resource_of_Project, name = 'project_resource'),
 
    # CRUD OPERATIONS ON Client Model
    re_path(r"^add_client/$", new_view.addClient, name="add_client"),
    re_path(r"^add_client_lead/$", new_view.addClientLead, name="add_client_lead"),
    re_path(r"^view_client/$", new_view.viewClient, name="view_client"),
    re_path(r"^view_client/update/(?P<id>\d+)/$", new_view.alterClient, name="alter_client"),
    re_path(r"^view_client/delete/(?P<id>\d+)/$", new_view.deleteClient, name="delete_client"),
    re_path(r"^client_projects/$", new_view.clientProjects, name="client_projects"),

    # url for client menu layout
    re_path(r"^resource_list/$", new_view.clientResourceList, name="resource_list"),
    re_path(r"^query_list/$", new_view.clientQuery, name="query_list"),
    re_path(r"^resource_worklog/$", new_view.resource_worklog, name="resource_worklog"),

    # url for view resource list in resource_lead user
    re_path(r"^lead_resourceList/$", new_view.Lead_resourceList, name="lead_resourceList"),

    # CRUD OPERATIONS ON Attend Model
    re_path(r"^add_attend/$",new_view.addAttend,name="add_attend"),
    re_path(r"^view_attends/$",new_view.viewAttend,name="view_attend"),
    re_path(r"^view_attend/update/(?P<id>\d+)/$",new_view.updateAttend,name="alter_attend"),
    re_path(r"^view_attend/delete/(?P<id>\d+)/$",new_view.deleteAttend,name="delete_attend"),
    re_path(r"^resource_attend/$",new_view.resourceAttend,name="resource_attend"),
    re_path(r"^lead_self_attend/$",new_view.lead_self_attend,name="lead_self_attend"),
    re_path(r"^get_attendance_status/$", new_view.get_attendance_status, name="get_attendance_status"),
    re_path(r"^update_attend_status/$", new_view.update_attend_status, name="update_attend_status"),

    # CRUD OPERATIONS ON ResourceDetail Model
    re_path(r"^addResource/$", new_view.addResourceProject, name="addResourceProject"),
    re_path(r"^viewResource/$", new_view.viewResource, name="viewResource"),
    re_path(r"^viewResource/update/(?P<id>\d+)/$", new_view.updateResourceProject, name="updateResourceProject"),
    re_path(r"^viewResource/delete/(?P<id>\d+)/$", new_view.deleteResourcePage, name="delete_resource"),
    re_path(r"^resource_projects/$", new_view.resourceProjects, name="resource_projects"),
    re_path(r"^view_list/$", new_view.viewList, name="view_list"),
    re_path(r"^rm_query/$", new_view.resourceRQD, name="rm_query"),
    

    # CRUD OPERATIONS ON ResourceProjectDetail Model
    re_path(r"^addResourceProjectDetail/$", new_view.addResourceProjectDetail, name="addResourceProjectDetail"),
    re_path(r"^viewResourceProjectDetail/$", new_view.viewResourceProjectDetail, name="viewResourceProjectDetail"),
    re_path(r"^viewResourceProjectDetail/update/(?P<id>\d+)/$", new_view.updateResourceProjectDetail,
            name="update_Detail"),
    re_path(r"^viewResourceProjectDetail/delete/(?P<id>\d+)/$", new_view.deleteResourceProjectDetail,
            name="delete_Detail"),

    # CRUD OPERATIONS ON RaisedQueryDetail Model
    re_path(r"^addRQD/$", new_view.addRQD, name="addRQD"),
    re_path(r"^viewRQD/$", new_view.viewRQD, name="viewRQD"),
    re_path(r"^viewRQD/update/(?P<id>\d+)/$", new_view.updateRQD, name="updateRQD"),
    re_path(r"^viewRQD/delete/(?P<id>\d+)/$", new_view.deleteRQD, name="delete_RQD"),
    # CRUD OPERATIONS FOR BLOG MODEL
    re_path(r"^add_blog/$", new_view.addBlog, name="add_blog"),
    re_path(r"^view_blog/$", new_view.viewBlog, name="view_blog"),
    re_path(r"^view_blog/update/(?P<id>\d+)/$", new_view.alterBlog, name="update_blog"),
    re_path(r"^view_blog/delete/(?P<id>\d+)/$", new_view.deleteBlog, name="delete_blog"),

    
    

    # CRUD OPERATIONS FOR WORKLOG MODEL
    re_path(r"^add_worklog/$", new_view.addWorkLog, name="add_worklog"),
    re_path(r"^view_worklog/$", new_view.viewWorklog, name="view_worklog"),
    re_path(r"^view_worklog/update/(?P<id>\d+)/$", new_view.updateWorklog, name="update_worklog"),
    re_path(r"^view_worklog/delete/(?P<id>\d+)/$", new_view.deleteWorklog, name="delete_worklog"),
    re_path(r"^total_resource_attend/$", new_view.resource_attend_count, name="total_resource_attend"),
    re_path(r"^worklog_list_not_add/$", new_view.worklog_list_not_add, name="worklog_list_not_add"),

    # CRUD URL FOR LEAD MODEL FORM

    re_path(r"^add_lead/$", new_view.addLead, name="add_lead"),
    re_path(r"^view_lead/$", new_view.viewLead, name="view_lead"),
    re_path(r"^view_lead/update/(?P<id>\d+)$", new_view.updateLead, name="update_lead"),
    re_path(r"^view_lead/delete/(?P<id>\d+)$", new_view.deleteLead, name="delete_lead"),

    #  Update LeadStatus Only
    re_path(r"^get_lead_status_form/$", lead_follow_up_update.get_lead_status_form, name="get_lead_status_form"),
    re_path(r"^update_lead_status/$", lead_follow_up_update.update_lead_status, name="update_lead_status"),
    
    # CRUD URL FOR LEAD follow up MODEL FORM
    re_path(r"^add_lead_fu/$", new_view.addLeadFU, name="add_lead_fu"),
    re_path(r"^view_lead_fu/$", new_view.viewLeadFU, name="view_lead_fu"),
    re_path(r"^view_lead_fu/update/(?P<id>\d+)$", new_view.updateLeadFU, name="update_lead_fu"),
    re_path(r"^view_lead_fu/delete/(?P<id>\d+)$", new_view.deleteLeadFU, name="delete_lead_fu"),
    
    re_path(r"^user_profile/$",new_view.userProfile,name="user_profile"),
    re_path(r"^view_project_fu/$", new_view.viewFUofLead, name="view_project_fu"),
    

    # List of Project with RQD
    re_path(r"^view_project_rqd/$", new_view.viewProj_with_RQD, name="view_project_rqd"),

    # This Url for Calendar
    re_path(r"^calendar/$", new_view.calendarView, name="calendar"),

    # CRUD OPERATION WITH LEAVE CALENDAR
    re_path(r"^add_leave_calendar/$", new_view.add_leave_calendar, name="add_leave_calendar"),
    re_path(r"^view_leave_calendar/$", new_view.view_leave_calendar, name="view_leave_calendar"),
    re_path(r"^view_leave_calendar/update/(?P<id>\d+)$", new_view.update_leave_calendar, name="update_leave_calendar"),
    re_path(r"^view_leave_calendar/delete/(?P<id>\d+)$", new_view.delete_leave_calendar, name="delete_leave_calendar"),
    re_path(r"^resource_leave_calendar/$", new_view.resource_leave_calendar, name="resource_leave_calendar"),


    #  Leave Modal urls
    re_path(r"^add_leave/$", leave_model_view.add_leave, name="add_leave"),
    re_path(r"^view_leave/$", leave_model_view.view_leave, name="view_leave"),
    re_path(r"^view_your_leave/$", leave_model_view.view_leave_resource, name="view_your_leave"),
    re_path(r"^view_leave/update/(?P<id>\d+)$", leave_model_view.update_leave, name="update_leave"),
    re_path(r"^view_leave/delete/(?P<id>\d+)$", leave_model_view.delete_leave, name="delete_leave"),


    #  Update LeadStatus Only
    re_path(r"^get_leave_status/$", leave_model_view.get_leave_status, name="get_leave_status"),
    re_path(r"^update_leave_status/$", leave_model_view.update_leave_status, name="update_leave_status"),

    # TASK MANAGMENT 
    re_path(r"^task_board/$",new_view.task_board,name="task_board"),
    re_path(r"^task_board/(?P<slug>[-\w]+)/$",new_view.view_task,name="view_task"),
    re_path(r"^add_sprint/$", new_view.add_sprint, name="add_sprint"),

    # resource progress     
    re_path(r"^resource_progress/(?P<id>\d+)$",new_view.resource_progress, name="resource_progress"),
    re_path(r"^resource_progress_list/$",new_view.resource_progress_list, name="resource_progress_list"),
    
    # View Apis Details
    path('view_applicant/', new_view.view_job_applicant, name='view_applicant'),
    path('view_web_queries/', new_view.view_web_queries, name='view_web_queries'),
    path('view_subscriber/', new_view.view_subscriber, name='view_subscriber'),
    # Download file
    path("view_file/<str:projectname>/", new_view.viewFile,name="view_files"),   
    # Un Authorized Url
    path("unauthorized/",new_view.unAuthorized,name="un_authorized"), 
    # Reset Password
    re_path(r"^reset_password/$",new_view.reset_password,name="reset_password"),
    path("set_password/<uidb64>/<token>/",new_view.set_password,name="set_password"),
    # URLS for Apis
    
    path("apis/", include(router.urls)),
    
 
    
]

