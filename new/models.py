from enum import unique
from django.db import models
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractBaseUser, User
import datetime
import os
from djmoney.models.fields import MoneyField
from ckeditor.fields import RichTextField


# Represents our ResourceDetail Model
class ResourceDetail(AbstractBaseUser, models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email_id = models.EmailField(unique=True)
    Python = "Python"
    PHP = "PHP"
    Wordpress = "Wordpress"
    PHPWordpress = "PHP+Wordpress"
    HTMLCSS = "HTML+CSS"
    NodeJs = "NodeJs"
    ReactJs = "ReactJs"
    ReactNativeJs = "ReactNativeJs"
    AngularJs = "AngularJs"
    TECHNOLOGY_CHOICES = [
        (Python, "Python"),
        (PHP, "PHP"),
        (Wordpress, "Wordpress"),
        (PHPWordpress, "PHP+Wordpress"),
        (HTMLCSS, "HTML+CSS"),
        (NodeJs, "NodeJs"),
        (ReactJs, "ReactJs"),
        (ReactNativeJs, "ReactNativeJs"),
        (AngularJs, "AngularJs"),
    ]
    technology = models.CharField(max_length=15, choices=TECHNOLOGY_CHOICES, null=True, blank=True)
    JSE = "Junior Software Engineer"
    FE_SE = "Frontend Software Engineer"
    FE_SSE = "Frontend Senior Software Engineer"
    FE_TL = "Frontend Team Lead"
    BE_SE = "Backend Software Engineer"
    BE_SSE = "Backend Senior Software Engineer"
    BE_TL = "Backend Team Lead"
    SA = "Solution Architect"
    DevOps = "DevOps"
    BA = "Business Analyst"
    QA = "Quality Analyst"
    PM = "Project Manager"
    LG = "Lead Generator"
    BDE = "Business Development Execute"
    HR = "HumanResource"
    ROLE_CHOICE = [
        (JSE, "Junior Software Engineer"),
        (FE_SE, "Frontend Software Engineer"),
        (FE_SSE, "Frontend Senior Software Engineer"),
        (FE_TL, "Frontend Team Lead"),
        (BE_SE, "Backend Software Engineer"),
        (BE_SSE, "Backend Senior Software Engineer"),
        (BE_TL, "Backend Team Lead"),
        (SA, "Solution Architect"),
        (DevOps, "DevOps"),
        (BA, "Business Analyst"),
        (QA, "Quality Analyst"),
        (PM, "Project Manager"),
        (LG, "Lead Generator"),
        (BDE, "Business Development Execute"),
        (HR, "Human Resource")

    ]
    role = models.CharField(max_length=50, choices=ROLE_CHOICE)
    password = models.CharField(max_length=200)
    contact = models.CharField(max_length=12)
    last_login = models.DateTimeField(auto_now=True)
    address = models.CharField(max_length=250)
    empolyee_id = models.CharField(unique=True, max_length=10)
    tds = models.BooleanField(default=False)
    salary = MoneyField(max_digits=14, decimal_places=2, default_currency='INR', null = True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email_id'
    REQUIRED_FIELDS = []

    def save(self, *args, **kwargs):
        """ This method is used to modify the password field
        converting text into hashed key"""
        try:
            pre_user = ResourceDetail.objects.last()
            self.empolyee_id = f"WSE-{(int(pre_user.id)+1):0=3d}"
        except:
            self.empolyee_id = "WSE-00"+str(1)

        self.password = make_password(self.password)
        super(ResourceDetail, self).save(*args, **kwargs)

    """Restricting access for end users to access
    admin page.Returns True only for superusers"""

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    """Restricting access for end users to access
    admin page.Returns True only for superusers"""

    def has_module_perms(self, app_label):
        return self.is_superuser

    def __str__(self):  # DUNDER METHOD
        """
            it is used to customize the string representation
            of the instance of class
        """
        return self.first_name + " " + self.last_name

    def email(self):
        return self.email_id

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_at']


# Represents our Client Detail Model
class ClientDetail(AbstractBaseUser, models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email_id = models.EmailField(unique=True)
    password = models.CharField(max_length=200, null=True, blank=True)
    business = models.CharField(max_length=50, null=True, blank=True)
    position = models.CharField(max_length=200, null=True, blank=True)
    contact = models.CharField(max_length=12)
    alternate_contact = models.CharField(max_length=12, null=True, blank=True)
    last_login = models.DateTimeField(auto_now=True)
    address = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    created_by = models.CharField(max_length=100, default='superadmin')

    USERNAME_FIELD = 'email_id'
    REQUIRED_FIELDS = []

    def save(self, *args, **kwargs):
        if self.password != None :
            """ This method is used to modify the password field
            converting text into hashed key"""
            self.password = make_password(self.password)
            super(ClientDetail, self).save(*args, **kwargs)
        else:
            self.password = None
            super(ClientDetail, self).save(*args, **kwargs)

    """Restricting access for end users to access
    admin page.Returns True only for superusers"""

    def has_perm(self, perm, obj=None):
        return self.is_superuser

    """Restricting access for end users to access
    admin page.Returns True only for superusers"""

    def has_module_perms(self, app_label):
        return self.is_superuser

    def __str__(self):  # DUNDER METHOD
        """
            it is used to customize the string representation
            of the instance of class 
        """
        return self.first_name

    def email(self):
        return self.email_id

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_at']


class ProjectDetail(models.Model):
    Pending = "Pending"
    Active = "Active"
    InActive = "Inactive"
    Testing = "Testing"
    Released = "Released"
    STATUS_CHOICES = [
        (Pending, "pending"),
        (Active, "active"),
        (InActive, "inActive"),
        (Testing, "testing"),
        (Released, "released"),
    ]
    name = models.CharField(max_length=200)
    technology = models.CharField(max_length=400, null=True)
    description = models.TextField()
    files = models.FileField(upload_to="project-files", null=True, blank=True)
    client = models.ForeignKey(ClientDetail, on_delete=models.CASCADE)
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default=Pending)
    milestone1 = models.CharField(max_length=150)
    mile = models.CharField(max_length=150)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):  # DUNDER METHOD
        """
            it is used to customize the string representation
            of the instance of class 
        """
        return self.name

    def no_of_resource(self):  # Method to get number of resource

        """it is used for, to see
         no. of resource on project details"""

        return ResourceProjectDetail.objects.filter(project=self).count()

    def project_raised_query(self):
        """ This method will be used to
        get the raised queries for project"""

        return RaisedQueryDetail.objects.filter(project=self).count()

    def filename(self):
        """This method will return only filename not full path"""

        return os.path.basename(self.files.name)

    def resource_detail(self):
        """This methode will return name
        of resource details on resource_list"""

        return ResourceProjectDetail.objects.filter(project=self).first()

    def resource_list(self):
        """ This Method will return no
         of resources on project"""
        role = ResourceDetail.objects.all()
        return ResourceProjectDetail.objects.filter(project=self, resource__in=role)

    def get_raisedquery_list(self):
        """This Method will return total no. of 
        raised query for project"""

        return RaisedQueryDetail.objects.filter(project=self).count()

    # returning project title
    def get_project_title(self):
        return self.pk

    # returnig RQD of project
    def get_project_rqd(self):
        project = self.get_project_title()
        return RaisedQueryDetail.objects.filter(project=project)

    def email(self):
        return self.email_id

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_at']


# Represents our Resource Project Detail Model
class ResourceProjectDetail(models.Model):
    project = models.ForeignKey(ProjectDetail, on_delete=models.CASCADE)
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)

    def __str__(self):
        return self.resource.first_name

    def get_resource(self):
        return self.resource

    def get_project(self):
        return self.project

    def get_attend_list(self):
        resource = self.get_resource()
        project = self.get_project()
        return Attend.objects.filter(resource=resource, project=project)

    def email(self):
        return self.email_id
    
    class Meta:
        unique_together = ('project', 'resource',)


# Represents our Raised Query Detail Model
class RaisedQueryDetail(models.Model):
    project = models.ForeignKey(ProjectDetail, on_delete=models.CASCADE)
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    description = models.TextField()
    files = models.FileField(upload_to="query-files", null=True, blank=True)

    def filename(self):
        """This method will return only filename not full path"""

        return os.path.basename(self.files.name)


# Represents our Attend Model
class Attend(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    date = models.DateField(default=datetime.datetime.now, editable=True, blank=False)
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    project = models.ForeignKey(ProjectDetail, on_delete=models.CASCADE)
    Pending = 'Pending'
    Approved = 'Approved'
    Rejected = 'Rejected'
    STATUS_CHOICE = {
        (Pending, 'Pending'),
        (Approved, 'Approved'),
        (Rejected, 'Rejected'),
    }
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default=Pending)

    def __str__(self):
        return self.resource.first_name

    def email(self):
        return self.email_id

    class Meta:
        """ this methode used for show recent data in tables """
        unique_together = ('date', 'resource')
        ordering = ['-updated_at']


# Represent our Blog Model
class Blog(models.Model):
    title = models.CharField(max_length=200)
    category = models.CharField(max_length=100)
    description = RichTextField()
    specific_keyword = models.CharField(max_length=200)
    image = models.ImageField(upload_to="Blog-images")
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    ip = models.GenericIPAddressField(blank=True, null=True)

    def __str__(self):
        return self.title

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_date']


# Represents our JobApplicant Model
class JobApplicant(models.Model):
    WordPress_Developer = 'WorldPress Developer'
    Angular_Developer = 'Angular Developer'
    React_Developer = 'React Developer'
    Laravel_Developer = 'Laravel Developer'
    Php_Developer = 'Php Developer'
    Front_end_Developer = 'Front-end Developer'
    Performance_Marketing_Specialists = 'Performance Marketing Specialists'
    Other = 'Other'
    OPTIONS = {
        (WordPress_Developer, 'WorldPress Developer'),
        (Angular_Developer, 'Angular Developer'),
        (React_Developer, 'React Developer'),
        (Laravel_Developer, 'Laravel Developer'),
        (Front_end_Developer, 'Front end Developer'),
        (Performance_Marketing_Specialists, 'Performance Marketing Specialists'),
        (Other, 'Other'),
    }
    hire = models.CharField(max_length=100, choices=OPTIONS, null=True, blank=True)
    Less_than_3 = 'Less than 3'
    three_to_five = '3 to 5'
    React_five_plus = 'React 5+'
    EXPERIENCE_OPTIONS = {
        (Less_than_3, 'Less than 3'),
        (three_to_five, '3 to 5'),
        (React_five_plus, 'React 5+'),
    }
    experience = models.CharField(max_length=100, choices=EXPERIENCE_OPTIONS, null=True, blank=True)
    Asap_just_looking_for_the_right_person = 'Asap, just looking for the right person'
    thirty_days_i_am_exploring_options = '30 days I am exploring options'
    Other = 'Other'
    POSITION_OPTIONS = {
        (Asap_just_looking_for_the_right_person, 'Asap, just looking for the right person'),
        (thirty_days_i_am_exploring_options, '30 days I am exploring options'),
        (Other, 'Other'),
    }
    positions = models.CharField(max_length=100, choices=POSITION_OPTIONS, null=True, blank=True)
    roles = models.CharField(max_length=250, null=True, blank=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.EmailField(unique=True)
    phone_no = models.CharField(max_length=12, unique=True)
    post = models.CharField(max_length=200)
    message = models.TextField()
    file = models.FileField(upload_to="jobApplicant-files")
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    ip = models.GenericIPAddressField(blank=True, null=True)

    def __str__(self):
        return self.first_name

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_date']


# Represents our WebQuery Model
class WebQueries(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.EmailField(unique=True)
    phone_no = models.CharField(max_length=12, unique=True)
    requirement = models.CharField(max_length=200)
    message = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    ip = models.GenericIPAddressField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "WebQueries"

    def __str__(self):
        return self.first_name

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_date']


# Represents our worklog Log Model

class Worklog(models.Model):
    pms_ticket_id = models.CharField(max_length=5)
    pms_ticket_link = models.URLField()
    description = models.TextField()
    logged_hour = models.IntegerField()
    project = models.ForeignKey(ProjectDetail, on_delete=models.CASCADE)
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    date = models.DateField(auto_now=True)

    def __str__(self):
        return self.pms_ticket_id

    def email(self):
        return self.email_id

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-date']


# Lead model presentation
class LeadModel(models.Model):
    Contacted = 'Contacted'
    RequirmentGathered = 'Requirment Gathered'
    SOWSubmited = 'SOW Submited'
    DemoDone = 'Demo Done'
    POGenerated = 'Purchase Order Generated'
    Hold = 'Hold'
    Rejected = 'Rejected'
    
    STATUS_CHOICE = {
        (Contacted, 'Contacted'),
        (RequirmentGathered, 'Requirment Gathered'),
        (SOWSubmited, 'SOW Submited'),
        (DemoDone, 'Demo Done'),
        (POGenerated, 'Purchase Order Generated'),
        (Hold, 'Hold'),
        (Rejected, 'Rejected'),
    }
    title = models.CharField(max_length=50)
    description = models.TextField()
    expected_amount = MoneyField(max_digits=14, decimal_places=2, default_currency='INR')
    added_by = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name="added")
    managed_by = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name="managed")
    status = models.CharField(choices=STATUS_CHOICE, default=Contacted, max_length=40)
    client = models.ForeignKey(ClientDetail, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_lead(self):
        return self.pk

    def get_lead_followUP(self):
        lead = self.get_lead()
        return LeadFollowUP.objects.filter(lead=lead)

    def get_status(self):
        return self.status

    def get_client(self):
        return self.client

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-created_at']


#  Lead Follow UP model
class LeadFollowUP(models.Model):
    lead = models.ForeignKey(LeadModel, on_delete=models.CASCADE)
    comment = models.CharField(max_length=500)
    file = models.FileField(upload_to='lead-follow-up', blank=True, null=True)
    added_by = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)

    def filename(self):
        """This method will return only filename not full path"""

        return os.path.basename(self.file.name) if self.file else ''

    def file_url(self):
        return self.file.url if self.file else ''


#  Leave Modal 
class leaveModel(models.Model):
    Pending = 'Pending'
    Approved = 'Approved'
    Rejected = 'Rejected'
    STATUS_CHOICE = {
        (Pending, 'Pending'),
        (Approved, 'Approved'),
        (Rejected, 'Rejected'),
    }
    Casual_Leave = "Casual Leave"
    Restricted_Leave = "Restricted Leave"
    Unpaid_Leave = "Unpaid Leave"
    LEAVE_TYPE_CHOICES = {
        (Casual_Leave, "Casual Leave"),
        (Restricted_Leave, "Restricted Leave"),
        (Unpaid_Leave, "Unpaid Leave")
    }
    start_date = models.DateField()
    end_date = models.DateField()
    reason = RichTextField()
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name="resource")
    comment = models.CharField(max_length=200)
    manager = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name="manager")
    leave_type = models.CharField(max_length=20, choices=LEAVE_TYPE_CHOICES, default=Casual_Leave)
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default=Pending)
    added_by = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_at']


# This Modal Created For Leave Calendar
class LeaveCalendar(models.Model):
    Yes = "Yes"
    No = "No"
    CHOICES = [
        (Yes, "Yes"),
        (No, "No"),
    ]
    date = models.DateField(default=datetime.datetime.now, editable=True, blank=False)
    day = models.CharField(max_length=50)
    event = models.CharField(max_length=50)
    national_holiday = models.CharField(max_length=8, choices=CHOICES)
    local_holiday = models.CharField(max_length=8, choices=CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.event

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-date']


# This Model Created For Board (Task Management)
class Board(models.Model):
    project = models.ForeignKey(ProjectDetail, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField()
    git_url = models.URLField(max_length=200)

    def __str__(self):
        return self.name
    
    def get_tasks(self):
        tasks = Task.objects.filter(board=self.pk, end_date__gte=datetime.date.today())
        print(tasks)
        return tasks


# This Model Created For Resource Board (Task Management)
class ResourceBoard(models.Model):
    Admin = "Admin"
    QA = "QA"
    Developer = "Developer"
    Manager = "Manager"
    ROLE_CHOICE = [
        (Admin, "admin"),
        (QA, "QA"),
        (Developer, "developer"),
        (Manager, "manager")
    ]
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    role = models.CharField(max_length=10, choices=ROLE_CHOICE, default=False)

    def get_board_list(self, resource):
        """ This method will return list of board objects """

        board_list = ResourceBoard.objects.filter(resource=resource)
        resource_board_list = []
        for boad in board_list:
            new_board = Board.objects.get(name=boad.board.name)
            resource_board_list.append(new_board)
        return resource_board_list

    def __str__(self):
        return str(self.board.name) + " - " + str(self.resource)


# Represents our Sprint Model

class Sprint(models.Model):
    name = models.CharField(max_length=100)
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    start_date = models.DateField(default=datetime.datetime.now, editable=True, blank=True)
    end_date = models.DateField(default=datetime.datetime.now, editable=True, blank=True)

    def __str__(self):
        return f"{self.name}-{self.board.name}"

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-end_date']


# This Model Created For Task (Task Management)
class Task(models.Model):
    ToDo = "ToDo"
    InProgress = "In-Progress"
    InReview = "In-Review"
    ReWork = "Re-Work"
    QA = "QA"
    Done = " Done"
    WORK_STATUS = [
        (ToDo, "ToDo"),
        (InProgress, "In-Progress"),
        (InReview, "In-Review"),
        (ReWork, "Re-Work"),
        (QA, "QA"),
        (Done, "Done")
    ]
    TASK = "Task"
    BUG = "Bug"
    STORY = "Story"
    EPIC = "Epic"
    TASK_TYPE = [
        (TASK, "Task"),
        (BUG, "Bug"),
        (STORY, "Story"),
        (EPIC, "Epic")
    ]
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    description = RichTextField()
    task_type = models.CharField(max_length=6, choices=TASK_TYPE, default=TASK)
    status = models.CharField(max_length=12, choices=WORK_STATUS, default=ToDo)
    assigned = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name='author')
    reporting = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name='reporter')
    parent_task = models.ForeignKey('self', blank=True, null=True, related_name='children', on_delete=models.DO_NOTHING)
    label = models.CharField(max_length=100)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE)
    start_date = models.DateField(default=datetime.datetime.now, editable=True, blank=True)
    end_date = models.DateField(default=datetime.datetime.now, editable=True, blank=True)
    slug_task = models.CharField(max_length=5, default="slug")

    HOURS = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('5', '5'),
        ('8', '8'),
        ('13', '13'),
    )
    story_hour = models.CharField(choices=HOURS, default='1', max_length=2)
    High = "High"
    Medium = "Medium"
    Low = "Low"
    PRIORITY = [
        (High, "High"),
        (Medium, "Medium"),
        (Low, "Low"),
    ]
    priority = models.CharField(choices=PRIORITY, default="Medium", max_length=7)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-created_at']


# This Model Created For Task Comment (Task Management)
class TaskComment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    comment = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-updated_at']


# This Model Created For Sprint (Task Management)


# This Model Created For Subscriber
class Subscriber(models.Model):
    email = models.EmailField(unique=True)
    ip = models.GenericIPAddressField(blank=True, null=True)


# this model for Resource Progres details
class ResourceProgress(models.Model):
    OUTSTANDING = "OUTSTANDING"
    EXCELLENT = "EXCELLENT"
    GOOD = "GOOD"
    AVERAGE = "AVERAGE"
    BELOW_AVERAGE = "BELOW AVERAGE"

    GRADE_CHOICES = {
        (OUTSTANDING, "OUTSTANDING"),
        (EXCELLENT, "EXCELLENT"),
        (GOOD, "GOOD"),
        (AVERAGE, "AVERAGE"),
        (BELOW_AVERAGE, "BELOW AVERAGE"),
    }

    resource = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    sprint = models.ForeignKey(Sprint, on_delete=models.CASCADE)
    grade = models.CharField(choices=GRADE_CHOICES, max_length=15, default=EXCELLENT)
    percentage = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self) -> str:
        return str(self.resource) + " -> Percentage: " + str(self.percentage)

    class Meta:
        """ this methode used for show recent data in tables """
        ordering = ['-created_at']
