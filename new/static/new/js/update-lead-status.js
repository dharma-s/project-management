$(document).on('click', '#updateLeadStatus', function(e) {
    var lead_id = $(this).data('lead-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: {
            'lead_id': lead_id,
            'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        },
        dataType: 'json',
        beforeSend: function() {
            $("#popModelSatatus").modal("show");
        },
        success: function(data) {
            console.log(data)
            $("#popModelSatatus .modal-body").html(data.update);
        }
    });
});