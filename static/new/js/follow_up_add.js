$(document).on('click', '#viewLeadAdd', function(e) {
    var url = $(this).data('url');
    var lead_id = $(this).data('lead-id');
    $.ajax({
        url: url,
        type: 'get',
        data: {'lead_id':lead_id, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val()},
        dataType: 'json',
        beforeSend: function() {
            $("#popmodel").modal("show");
        },
        success: function(data) {
            $("#popmodel .modal-body").html(data.form);
        }
    });
});