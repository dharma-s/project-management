$(document).on('click', '#viewProjRQD', function(e) {
    var project_id = $(this).data('project-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: { 'project_id': project_id, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val() },
        dataType: 'json',
        beforeSend: function() {
            $("#popModelRQD").modal("show");
        },
        success: function(data) {
            $("#popModelRQD .modal-body").html(data.html);
        }
    });
});