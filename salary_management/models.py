import datetime
from django.db import models
from django.shortcuts import get_object_or_404
from new.models import ResourceDetail
from djmoney.models.fields import MoneyField

# Create your models here.



class EmployeeAccountDetail(models.Model):
    '''employee bank account detail table'''
    employee = models.OneToOneField(ResourceDetail, on_delete=models.CASCADE)
    bank_name = models.CharField(max_length=50)
    ifsc = models.CharField(max_length=11)
    account_number = models.BigIntegerField()
    pan = models.CharField(max_length=10, blank=True, null=True)
    name_as_bank_detail = models.CharField(max_length=100)
    bank_account_status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self) -> str:
        return str(self.employee)+" -> "+str(self.account_number)
    
    class Meta:
        ordering = ['-created_at']



class EmployeeSalary(models.Model):
    '''Employee salary structure table'''
    
    New_Joining = "New Joining"
    Apprasial = "Apprasial"
    
    EMP_TYPES=[
        (New_Joining, "New Joining"),
        (Apprasial, "Apprasial"),
    ]
    
    employee = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE)
    basic_salary = MoneyField(max_digits=14, default_currency='INR')
    hra = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    conveyance = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    special_allowance = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    fuel = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    ta = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    medical = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    leave_encashment = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    telephone = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    performence_pay = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    arrear = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    pf = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    gratuity = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    skill_development = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    total_salary = MoneyField(max_digits=14, default_currency='INR', default=0)
    
    employment_type = models.CharField(choices=EMP_TYPES, default=New_Joining, max_length=20)
    joining_date = models.DateField(null=True, blank=True)
    last_apprasial_date = models.DateField(null=True, blank=True)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self) -> str:
        return str(self.employee) + " -> " + str(self.total_salary)
    
    class Meta:
        ordering = ['-created_at']
        unique_together = ('employee', 'total_salary',)



class Salary(models.Model):
    '''monthaly salary details table of employee '''
    # get previus month last date start
    today = datetime.date.today()
    first = today.replace(day=1)
    lastMonth = first - datetime.timedelta(days=1)
    # end
    
    employee = models.ForeignKey(ResourceDetail, on_delete=models.CASCADE, related_name="Employee")
    total_days = models.IntegerField()
    deductions = MoneyField(max_digits=14, default_currency='INR', blank=True, null=True)
    
    basic_salary = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    hra = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    conveyance = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    special_allowance = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    fuel = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    ta = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    medical = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    leave_encashment = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    telephone = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    performence_pay = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    arrear = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    pf = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    gratuity = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    skill_development = MoneyField(max_digits=14, default_currency='INR', null=True, blank=True)
    
    current_month_total_salary = MoneyField(max_digits=14, default_currency='INR')
    month = models.DateField(default=lastMonth)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return str(self.employee)+" -> "+str(self.current_month_total_salary)
    
    def salary(self):
        return str(self.current_month_total_salary)
    
    def get_bank_details(self):
        ac = get_object_or_404(EmployeeAccountDetail, employee=self.employee)
        if ac:
            return ac
        return False
    
    class Meta:
        unique_together = ('employee', 'month',)


