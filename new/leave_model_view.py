import datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.http import JsonResponse
from django.template.loader import render_to_string
from .forms import leaveModelForm
from .models import ResourceDetail, leaveModel
from .views import get_user_role



def add_leave(request):
    # Application for Leave 
    if request.method == "POST":
        form = leaveModelForm(request.POST)
        form.instance.date_start = request.POST.get('start_date')
        form.instance.date_end = request.POST.get('end_date')
        form.instance.added_by = request.user
        resource = request.POST.get('resource')
        leave_type = request.POST.get('leave_type')
        if form.is_valid():
            leave = current_year_leave_count(resource)
            if leave['casual_leave_taken'] >= 12 and leave_type == 'Casual Leave':
                messages.warning(request, "You have take all causal leave")
                return render(request, "new/leave/add_leave.html", context={"form": form})
            elif leave['restricted_leave_taken'] >= 2 and leave_type == 'Restricted Leave':
                messages.warning(request, "You have take all restricted leave")
                return render(request, "new/leave/add_leave.html", context={"form": form})
            elif form.instance.start_date > form.instance.end_date:
                messages.warning(request, "Date Input Invalid..!")
                return render(request, "new/leave/add_leave.html", context={"form": form})
            form.save()
            messages.success(request, "Application Submitted, wait for approval...!")
            return redirect("view_leave")
        else:
            return render(request, "new/leave/add_leave.html", context={"form": form})
    else:
        form = leaveModelForm()
        return render(request, "new/leave/add_leave.html", context={"form": form})


def view_leave(request):

    # View Leave Applications for every user
    user_role = get_user_role(request, request.user)
    lead_list = ['superadmin', 'project_manager', 'solution_architect', 'frontend_team_lead', 'backend_team_lead', 'business_analyst']
    
    if user_role in ['superadmin', 'human_resource']:
        all_leave = leaveModel.objects.all()
        return render(request, "new/leave/view_leave.html", context={"data": all_leave})
    
    elif user_role == 'project_manager' or user_role == 'business_analyst':
        resource = ResourceDetail.objects.filter(role__in = ['quality_analyst', 'devops', 'frontend_team_lead', 'backend_team_lead'])
        resource_leave = leaveModel.objects.filter(resource__in = resource, manager = request.user)
        
    elif user_role == 'quality_analyst':
        resource = ResourceDetail.objects.filter(role = 'solution_architect')
        resource_leave = leaveModel.objects.filter(resource__in = resource, manager = request.user)
        
    elif user_role == 'solution_architect':
        resource = ResourceDetail.objects.filter(role__in = ['frontend_team_lead', 'backend_team_lead'])
        resource_leave = leaveModel.objects.filter(resource__in = resource, manager = request.user)
        
    elif user_role in ['frontend_team_lead', 'backend_team_lead']:
        se_role_list = ["Junior Software Engineer", "Frontend Software Engineer", "Frontend Senior Software Engineer",
            "Backend Software Engineer", "Backend Senior Software Engineer"] 
        resource = ResourceDetail.objects.filter(role__in = se_role_list)
        resource_leave = leaveModel.objects.filter(resource__in = resource, manager = request.user)
        
    else:
        return redirect('view_your_leave')
    
    return render(request, "new/leave/view_leave.html", context={"data": resource_leave, 'user_role': user_role, 'lead_list': lead_list })



def view_leave_resource(request):
    try:
        leave = leaveModel.objects.filter(resource = request.user)
        return render(request, "new/leave/view_leave_resource.html", context={"data": leave, "leave_count" : current_year_leave_count(request.user)})
    except AttributeError:
        return redirect('index')


def update_leave(request, id):
    # update leave Aplication
    instance = get_object_or_404(leaveModel, id=id)
    form = leaveModelForm(request.POST or None, instance=instance)
    resource = request.POST.get('resource')
    leave_type = request.POST.get('leave_type')
    if request.method == "POST":
        if form.is_valid():
            leave = current_year_leave_count(resource)
            if leave['casual_leave_taken'] >= 12 and leave_type == 'Casual Leave':
                messages.warning(request, "You have take all causal leave")
                return render(request, "new/leave/update.html", {"form": form})
            elif leave['restricted_leave_taken'] >= 2 and leave_type == 'Restricted Leave':
                messages.warning(request, "You have take all restricted leave")
                return render(request, "new/leave/update.html", {"form": form})
            elif form.instance.start_date > form.instance.end_date:
                messages.warning(request, "Date Input Invalid..!")
                return render(request, "new/leave/update.html", {"form": form})
            form.save()
            messages.success(request, "Application Changed...! ")
            return redirect("view_leave")
        else:
            return render(request, "new/leave/update.html", {"form": form})
    return render(request, "new/leave/update.html", {"form": form})


def delete_leave(request, id):
    # Delete Specific Leave Application 
    instance = get_object_or_404(leaveModel, id=id)
    if request.method == "POST":
        instance.delete()
        messages.success(request, "Selected Leave deleted...")
        return redirect('view_leave')


def get_leave_status(request):
    # Get Leave Application using Json data
    try:
        leave_id = request.POST.get('leave_id')
        print(leave_id)
        html = render_to_string("new/leave/updateLeaveStatus.html", { 'leave_id': leave_id, }, request = request)
        return JsonResponse({'update': html})
    except:
        return redirect('view_leave')


def update_leave_status(request):
    if request.method == 'POST':
        leave_id = request.POST.get('leave_id_to_update')
        leave_status = request.POST.get('leave_status')
        leave_comment = request.POST.get('comment')
        instance = get_object_or_404(leaveModel, id = leave_id)
        instance.status = leave_status
        instance.comment = leave_comment
        instance.save()
        messages.success(request, "Application saved " )
        return redirect('view_leave')
    else:
        return redirect('view_leave')


'''this function return Taken leave and Remainning leave of Resource in this year'''
def current_year_leave_count(resource):
    max_restricted_leave_allowed = 2
    max_caual_leave_allowed = 12
    unpaid_leave = 0
    casual_leave_taken = 0
    restricted_leave_taken = 0
    today = datetime.date.today()
    leave_instance = leaveModel.objects.filter(start_date__year=today.year, resource=resource, status = "Approved")
   
    for leave in leave_instance:
        if leave.start_date == leave.end_date:
            if leave.leave_type == "Casual Leave":
                casual_leave_taken += 1
            elif leave.leave_type == "Unpaid Leave":
                unpaid_leave += 1
            else:
                restricted_leave_taken += 1
        else:
            end_date = leave.end_date
            start_date = leave.start_date
            total_days = end_date - start_date
            if leave.leave_type == "Casual Leave":
                casual_leave_taken += total_days.days
            elif leave.leave_type == "Unpaid Leave":
                unpaid_leave += total_days.days
            else:
                restricted_leave_taken += total_days.days
            
    context={
        "casual_leave_taken" : casual_leave_taken,
        "restricted_leave_taken":restricted_leave_taken,
        "casual_leave_remainning" : max_caual_leave_allowed-casual_leave_taken,
        "restricted_leave_remainning":max_restricted_leave_allowed-restricted_leave_taken,
        "total_leave_taken" : casual_leave_taken+restricted_leave_taken+unpaid_leave,
        "total_leave_remainning":max_restricted_leave_allowed - restricted_leave_taken + max_caual_leave_allowed - casual_leave_taken,
        "unpaid_leave": unpaid_leave
    }
    return context


def leave_count_of_month(resource, month, year):
    
    unpaid_leave, casual_leave_taken, restricted_leave_taken = 0, 0,0
    leave_instance = leaveModel.objects.filter(start_date__year=year, start_date__month=month, resource=resource, status__in = ["Pending","Approved"])
   
    for leave in leave_instance:
        if leave.start_date == leave.end_date:
            if leave.leave_type == "Casual Leave":
                casual_leave_taken += 1
            elif leave.leave_type == "Unpaid Leave":
                unpaid_leave += 1
            else:
                restricted_leave_taken += 1
        else:
            end_date = leave.end_date
            start_date = leave.start_date
            total_days = end_date - start_date
            if leave.leave_type == "Casual Leave":
                casual_leave_taken += total_days.days
            elif leave.leave_type == "Unpaid Leave":
                unpaid_leave += total_days.days
            else:
                restricted_leave_taken += total_days.days
            
    context={
        "casual_leave_taken" : casual_leave_taken,
        "restricted_leave_taken":restricted_leave_taken,
        "total_leave_taken" : casual_leave_taken+restricted_leave_taken+unpaid_leave,
        "unpaid_leave_taken": unpaid_leave
    }
    return context
