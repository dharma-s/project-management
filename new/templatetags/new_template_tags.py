
from django import template
from new.models import ResourceDetail, ClientDetail, ResourceProjectDetail
from django.contrib.auth.models import User
register = template.Library()


@register.filter('get_custom_role')
def get_custom_role(email, email_id):
    current_user_email = email if email_id is None else email_id
    try:
        user = ResourceDetail.objects.get(email_id=current_user_email)
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Project Manager').exists():
            return "project_manager"
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Frontend Team Lead').exists():
            return 'frontend_team_lead'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Business Analyst').exists():
            return 'business_analyst'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Solution Architect').exists():
            return 'solution_architect'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Lead Generator').exists():
            return 'lead_generator'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Business Development Execute').exists():
            return 'business_development_execute'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Backend Team Lead').exists():
            return 'backend_team_lead'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='HumanResource').exists():
            return 'human_resource'
        else:
            return 'resource_member'
    except ResourceDetail.DoesNotExist:
        try:
            ClientDetail.objects.get(email_id=current_user_email)
            return 'client'
        except ClientDetail.DoesNotExist:
            try:
                User.objects.get(email=current_user_email)
                print("das")
                return 'superadmin'
            except User.DoesNotExist:
                print("dfs")
                return None


@register.filter('print_custom_role')
def print_custom_role(email):
    current_user_email = email
    try:
        user = ResourceDetail.objects.get(email_id=current_user_email)
        # if ResourceProjectDetail.objects.filter(resource=user, role='Members').exists():
        #     return 'resource_member'
        # if ResourceProjectDetail.objects.filter(resource=user, role='Lead').exists():
        #     return 'resource_lead'
        # if ResourceProjectDetail.objects.filter(resource=user, role='BD').exists():
        #     return 'resource_bd'
        # if ResourceProjectDetail.objects.filter(resource=user, role='Surfer').exists():
        #     return 'resource_surfer'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Project Manager').exists():
            return 'Project Manager User'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Quality Analyst').exists():
            return 'Quality Analyst User'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='DevOps').exists():
            return 'Resource Member'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Frontend Team Lead').exists():
            return 'frontend_team_lead'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Business Analyst').exists():
            return 'Business_Analyst'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Solution Architect').exists():
            return 'Solution_Architect'
        if ResourceDetail.objects.filter(email_id=current_user_email,
                                           role='Frontend Senior Software Engineer').exists():
            return 'Resource Member'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Junior Software Engineer').exists():
            return 'Resource Member'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Frontend Software Engineer').exists():
            return 'Resource Member'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Backend Software Engineer').exists():
            return 'Resource Member'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Lead Generator').exists():
            return 'lead_generator'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Business Development Execute').exists():
            return 'business_development_execute'
        if ResourceDetail.objects.filter(email_id=current_user_email,
                                           role='Backend Senior Software Engineer').exists():
            return 'Resource Member'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Backend Team Lead').exists():
            return 'backend_team_lead'
        if ResourceDetail.objects.filter(email_id=current_user_email, role='HumanResource').exists():
            return 'human_resource'
        else:
            return 'resource_member'
    except ResourceDetail.DoesNotExist:
        try:
            ClientDetail.objects.get(email_id=current_user_email)
            return 'client'
        except ClientDetail.DoesNotExist:
            try:
                User.objects.get(email=current_user_email)
                return 'superadmin'
            except User.DoesNotExist:
                return None



