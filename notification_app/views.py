from django.http import JsonResponse
from django.shortcuts import render, HttpResponse
from notification_app.models import *
from new.models import *
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt


# This methode is showing notification on create new task
def add_task_notification_id(reporting, task):
    reporting = reporting
    reporting_id = reporting.id
    task = task
    notice = "Task created by " + str(reporting)
    notification = Notification(notification=notice, notification_type="Add",
                                admin_receiver=User.objects.all()[0], resource_receiver=task.assigned,
                                manager_receiver=task.reporting, status=False)
    notification.save()
    return True


# This methode is showing notification on update task
def update_task_notification(task):
    task_assigned = task.reporting
    task_name = task.slug_task + ' Task has been Updated by '
    notification = Notification(notification=task_name + str(task_assigned), notification_type="Update Task",
                                admin_receiver=User.objects.all()[0], resource_receiver=task.assigned,
                                manager_receiver=task.reporting, status=False)
    notification.save()
    return True


# This methode is showing notification, today is end date of task and status is not done
def task_end_date_notification():
    task = Task.objects.filter(end_date__gte=datetime.date.today()).exclude(status="Done")
    for x in task:
        notification = Notification(notification="Today is the end date of " + str(x.assigned) + " task ",
                                    notification_type="Status", admin_receiver=User.objects.all()[0],
                                    resource_receiver=x.assigned, manager_receiver=x.reporting, status=False)
        notification.save()
    return True


# This methode is showing notification on add new lead
def add_lead_notification(lead):
    lead_title = lead.title
    lead_add = lead.added_by
    notice = 'Added a Lead  to you in' + str(lead_add)
    lead_client = lead.client
    notification = Notification(notification=notice, notification_type="Add", admin_receiver=User.objects.all()[0],
                                client_receiver=lead_client, resource_receiver=lead_add,
                                manager_receiver=lead.managed_by, status=False)
    notification.save()
    return True


# This methode is showing notification on update lead
def update_lead_notification(update_lead, id):
    update_lead = update_lead
    added = update_lead.added_by
    notice = str(update_lead) + " lead has been updated by " + str(added)
    notification = Notification(notification=notice, notification_type="Update", admin_receiver=User.objects.all()[0],
                                client_receiver=update_lead.client, resource_receiver=added,
                                manager_receiver=update_lead.managed_by,
                                status=False)
    notification.save()
    return True


@csrf_exempt
def view_notification(request):
    role = get_user_role(request, request.user)
    user = request.user
    try:
        if request.is_ajax() and request.POST.get("action") == "get_count":
            if role == "superadmin":
                notification = Notification.objects.all()
                notification_count = Notification.objects.filter(admin_read_status=False)
            elif role in ["project_manager", "solution_architect", "frontend_team_lead", "backend_team_lead",
                          'business_analyst']:
                notification = Notification.objects.filter(manager_receiver=user)
                notification_count = Notification.objects.filter(manager_receiver=user, manager_read_status=False)
            elif role in ["business_development_execute", "lead_generator", "resource_member", "human_resource"]:
                notification = Notification.objects.filter(resource_receiver=user)
                notification_count = Notification.objects.filter(resource_receiver=user, resource_read_status=False)
            contex = {
                "notification": notification,
                "count": notification_count.count(),
            }
            html = render_to_string("new/notification/count.html", request=request, context=contex)
            html1 = render_to_string("new/notification/popUp.html", request=request, context=contex)
            return JsonResponse({"html": html, "html1": html1})

        if request.is_ajax() and request.POST.get("action") == "read_notification":
            if role == "superadmin":
                notification_count = Notification.objects.filter(admin_read_status=False)
                for read in notification_count:
                    read.admin_read_status = True
                    read.save()
            elif role in ["project_manager", "solution_architect", "frontend_team_lead", "backend_team_lead",
                          'business_analyst']:
                notification_count = Notification.objects.filter(manager_receiver=user, manager_read_status=False)
                for read in notification_count:
                    read.manager_read_status = True
                    read.save()
            elif role in ["business_development_execute", "lead_generator", "resource_member", "human_resource"]:
                notification_count = Notification.objects.filter(resource_receiver=user, resource_read_status=False)
                for read in notification_count:
                    read.resource_read_status = True
                    read.save()

            contex = {
                "count": '',
            }
            html = render_to_string("new/notification/count.html", request=request, context=contex)
            return JsonResponse({"html": html})
    except:
        pass


def get_user_role(request, user):
    try:
        current_user_email = user.email_id
    except:
        current_user_email = user.email
    try:
        user = ResourceDetail.objects.get(email_id=current_user_email)
        if ResourceDetail.objects.filter(email_id=current_user_email, role='Project Manager').exists():
            return 'project_manager'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Lead Generator').exists():
            return 'lead_generator'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Frontend Team Lead').exists():
            return 'frontend_team_lead'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Business Analyst').exists():
            return 'business_analyst'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Solution Architect').exists():
            return 'solution_architect'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Business Development Execute').exists():
            return 'business_development_execute'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='Backend Team Lead').exists():
            return 'backend_team_lead'
        elif ResourceDetail.objects.filter(email_id=current_user_email, role='HumanResource').exists():
            return 'human_resource'
        else:
            return "resource_member"
    except ResourceDetail.DoesNotExist:
        try:
            ClientDetail.objects.get(email_id=current_user_email)
            return 'client'
        except ClientDetail.DoesNotExist:
            try:
                User.objects.get(email=current_user_email)
                return 'superadmin'
            except User.DoesNotExist:
                return None
