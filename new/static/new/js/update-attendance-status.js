$(document).on('click', '#updateAttendStatus', function(e) {
    var attend_id = $(this).data('attend-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: {
            'attend_id': attend_id,
            'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        },
        dataType: 'json',
        beforeSend: function() {
            $("#popAttendStatus").modal("show");
        },
        success: function(data) {
            console.log(data)
            $("#popAttendStatus .modal-body").html(data.update);
        }
    });
});