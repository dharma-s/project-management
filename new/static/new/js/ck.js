function text_hide() {
    document.getElementById('cmt').style.display = "none";
    document.getElementById('editor').style.display = "block";
}

function open_field() {
    document.getElementById('cus_filed').style.display = "block";
}

function show_btn() {
    document.getElementById("shw").disabled = false;
}

function hde_btn() {
    document.getElementById("cus_filed").style.display = "none";
}

function hide_edtr() {
    document.getElementById('editor').style.display = "none";
    document.getElementById('cmt').style.display = "block";
}

function shw_btnn() {
    document.getElementById('shw_chkk').style.display = "block";
}

function cnt_hide() {
    if (document.getElementById('cmtt').style.display == "block") {
        document.getElementById('cmtt').style.display = "none";
        document.getElementById('editorw').style.display = "block";
    } else {
        document.getElementById('cmtt').style.display = "block";
        document.getElementById('editorw').style.display = "none";
    }
}

var allEditors = document.getElementById('ckeditor0');
ck = CKEDITOR.replaceAll(allEditors);




function hide_edtrcnt() {
    $("#editorw").hide();
    $('#cmtt').show();
    var newdf = document.getElementById('editorw');
    console.log(newdf);


}
$('#myButton').click(function() {
    $.scrollTo($('#myDiv'), 1000);
});

$('#myButton').click(function() {
    $.scrollTo($('#myDivlink'), 1000);
});


function view_task(parameter) {
    $.ajax({

        type: "POST",
        url: "/task_board/",
        data: {
            action: "view_task",
            id: parameter
        },
        success: function(data) {


            $("div#modelviewtask").html(data.task);


        }

    });

}




function search_task() {
    let input = document.getElementById('id_search').value
    input = input.toUpperCase();
    let x = document.getElementsByClassName('task_search');

    for (i = 0; i < x.length; i++) {

        if (!x[i].innerHTML.toUpperCase().includes(input)) {
            x[i].style.display = "none";
        } else {
            x[i].style.display = "list-item";
        }
    }
}



// this function is use to add comment on task from task view page
function add_commit(id) {

    // var comment = CKEDITOR.instances[''].getData();
    var comment = document.getElementById('ghfghf').value;
    if (comment == "") {
        return false;
    }
    $.ajax({
        type: "POST",
        url: "/task_board/",
        data: {
            action: "add_commit",
            id: id,
            comment: comment
        },
        success: function(data) {
            $("div#taskComment").html(data.task);
            document.getElementById('ghfghf').value = "";
        }
    });
}



// function to show edit comment box and hide comment
function comment_box(id) {

    var comment_div_id = "comment_editor_div_" + id;
    var comment_id = "id_comment_div_" + id;

    document.getElementById(comment_div_id).style.display = "block";
    document.getElementById(comment_id).style.display = "none";

}



function hide_comment_box(id) {
    // when click on cancle btn in edit comment box will hide and comment will visible

    var comment_div_id = "comment_editor_div_" + id;
    var comment_id = "id_comment_div_" + id;

    document.getElementById(comment_div_id).style.display = "none";
    document.getElementById(comment_id).style.display = "block";

}



function change_commit(task_id, comment_id, whatToDo) {

    // This function is use to Edit and Delete the Comment of task
    // and whatToDo takes 'edit' or 'delete' from html page 

    if (whatToDo == "edit") {
        var edited_comment_id = "edit_comment_input_" + comment_id; //use to det edited comment id 
        var comment = document.getElementById(edited_comment_id).value;
    } else {
        comment = "";
    }

    $.ajax({
        type: "POST",
        url: "/task_board/",
        data: {
            action: "change_commit",
            task_id: task_id,
            comment_id: comment_id,
            what_to_do: whatToDo,
            comment: comment
        },
        success: function(data) {
            $("div#taskComment").html(data.task);
        },
        error: function() {
            alert("Please Close task and reopen it..");
        }
    });
}

function set_id_delete(parameter) {

    // This function will add task id on delete popup

    $("#delete_task_id").val(parameter);
    event.preventDefault();
}

function delete_task() {

    //This function will delete task permanently

    var id = $("#delete_task_id").val();
    var board_name = $("#board_name").html();
    $("#deleteModel").modal("hide");
    $.ajax({
        type: "POST",
        url: "/task_board/",
        data: {
            action: "delete_task",
            delete_task_id: id,
            board_name: board_name
        },
        success: function(data) {

            $("div#task_sections").html(data.data);
        }
    });
}




// this js function is use to update-title,description,status,assigned,start_dare,end_date on task_view popup from task view page
$(document).on('click', '.UpdateTitle', function() {

    var title = document.getElementById('taskTitleId').value;
    var labels = document.getElementById('task_labels').value;
    var start_date = document.getElementById('task_start_date').value;
    var end_date = document.getElementById('task_end_date').value;


    var action = $(this).data("action");
    var id = $(this).data("id");
    var resource_id = $(this).data("resource-id");
    var data = {};
    if (action == 'update_title') {
        data = {
            action: 'update_title',
            id: id,
            task_title: title
        }
    } else if (action == 'update_status') {
        var status = $(this).data("status")
        data = {
            action: 'update_status',
            id: id,
            update_status: status
        }
    } else if (action == 'update_description') {
        var desc = CKEDITOR.instances['ckeditor0'].getData();
        data = {
            action: action,
            id: id,
            task_description: desc
        }
    } else if (action == 'update_assigned') {
        var resource_name = $(this).data("resource-name");
        data = {
            action: 'update_assigned',
            id: id,
            update_assigned: resource_id
        }
    } else if (action == 'update_labels') {
        data = {
            action: 'update_labels',
            id: id,
            update_labels: labels
        }
    } else if (action == 'update_start_date') {
        data = {
            action: 'update_start_date',
            id: id,
            start_date: start_date
        }
    } else if (action == 'update_end_date') {
        data = {
            action: 'update_end_date',
            id: id,
            end_date: end_date
        }
    } else if (action == 'update_story_hour') {
        var story_hour = $(this).data("story_hour")
        data = {
            action: 'update_story_hour',
            id: id,
            update_story_hour: story_hour
        }
    }
    $.ajax({

        type: 'POST',
        url: '/task_board/',
        data: data,
        success: function(data) {

            $('div#taskTitleId').html(data.task);
            $('#updateStatus').html(status);
            $('#TaskAssigned').html(resource_name);
            $('div#task_description').html(data.task);
            $('#TaskDescription').html(desc);
            $('#updateStoryHour').html(story_hour)
            hello_h();

        },


    });
    window.location.reload();
})


function hello_h() {
    document.getElementById("shw_chkk").style.display = 'none';
    document.getElementById("show_check").style.display = 'none';
    document.getElementById("show_start").style.display = 'none';
    document.getElementById("show_end").style.display = 'none';

}

function shw_butt() {
    document.getElementById('show_check').style.display = 'block';
}

function start_btn() {
    document.getElementById('show_start').style.display = 'block';

}

function end_btn() {
    document.getElementById('show_end').style.display = 'block';
}

function getURL() {
    navigator.clipboard.writeText(window.location.href);
    $("#jhhghg").html("Link Coppied");
    $("#jhhghg").css('border-color', '#04AA6D');
    $("#jhhghg").css('color', 'green');
}


$(document).on('click', '#is_addSprintBtn', function() {

    var url = $(this).data('url');
    var board_id = $("#id_task_board").find('option:selected').attr("name");
    $.ajax({
        url: url,
        type: 'post',
        data: {
            'board_id': board_id,
            'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        },

        dataType: 'json',
        beforeSend: function() {
            $("#addSprintModel").modal("show");
        },
        success: function(data) {
            if (data.permission) {
                $("#addSprintModel .modal-body").html("You have already a sprint for today, so try after: " + data.date);
            } else {
                $("#addSprintModel .modal-body").html(data.html);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
            $("#addSprintModel").modal("hide");
        }
    });
});


function get_attend() {

    var month = document.getElementById('month').value;
    var year = document.getElementById('year').value;
    var pathname = window.location.pathname;
    $.ajax({
        url: "/total_resource_attend/",
        data: {
            month: month,
            year: year,
            url: pathname
        },
        success: function(data) {
            $("div#attend_container").html(data.html);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });
}

function find_months() {

    var year = document.getElementById('year').value;
    $.ajax({
        url: "/total_resource_attend/",
        data: {
            action: "change_month",
            year: year
        },
        success: function(data) {
            $("div#month_div").html(data.html);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Error: " + errorThrown);
        }
    });


}


function notification_read() {
    $.ajax({
        type:"POST",
        url:"/notification/view_notification/",
        data:{
            action:"read_notification"
        },
        success: function(data){
            $("span#Count").html(data.html);
        }
    });
}

/*This Function is Showing Notification and notification count*/
function notification_count() {
  $.ajax({
        type:"POST",
        url:"/notification/view_notification/",
        data:{
            action:"get_count"
        },
        success: function(data){
            $("span#Count").html(data.html);
            $("div#noticeId").html(data.html1);
        }

  });
}

