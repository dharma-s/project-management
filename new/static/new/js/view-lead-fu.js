$(document).on('click', '#viewLeadFU', function(e) {
    var lead_id = $(this).data('lead-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: { 'lead_id': lead_id, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val() },
        dataType: 'json',
        beforeSend: function() {
            $("#popModel").modal("show");
        },
        success: function(data) {
            console.log(data)
            $("#popModel .modal-body").html(data.html);
        }
    });
});