$(document).on('click', '#updateLeaveStatus', function(e) {
    var leave_id = $(this).data('leave-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: {
            'leave_id': leave_id,
            'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        },
        dataType: 'json',
        beforeSend: function() {
            $("#popModelSatatus").modal("show");
        },
        success: function(data) {
            console.log(data)
            $("#popModelSatatus .modal-body").html(data.update);
        }
    });
});