from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.http import JsonResponse
from django.template.loader import render_to_string
from .models import LeadModel, ProjectDetail, ResourceProjectDetail, ResourceDetail
from .forms import updateLeadStatusForm


def get_lead_status_form(request):
    # pop model using Ajax
    lead_id = request.POST.get('lead_id')
    if request.method == 'POST':
        data = get_object_or_404(LeadModel, id=lead_id)
        form = updateLeadStatusForm(instance=data)
    html = render_to_string("new/lead/updateLeadStatus.html", {'form': form, 'lead_id': lead_id}, request=request)
    return JsonResponse({'update': html})


# method to create a project by passing data in dict
def create_project(request, project_data):
    try:
        project_form = ProjectDetail(name = project_data['name'], description = project_data['description'],
        client = project_data['client'], status = "Pending", technology = "", files = "")
        project_form.save()
        messages.success(request, "Project data added")
        return True
    except:
        messages.warning(request, "tried to save form but failed...")
        return False


def create_project_resource(request, pr):
    # method to create data in Project detail 
    try:
        project = ProjectDetail.objects.get(name = pr['name'], client = pr['client'])
        b_d_e = ResourceDetail.objects.filter(role__in='Business Development Execute')
        pr_form = ResourceProjectDetail(project=project, resource=b_d_e)
        pr_form.save()
        messages.success(request, "Resource Project Detail saved...,You are Resource role as BD")
        return True
    except:
        messages.warning(request, "tried to save form but failed...")
        return False


def update_lead_status(request):
    if request.method == 'POST':
        lead_id = request.POST.get('lead_id_to_update')
        lead_status = request.POST.get('lead_status')
        instance = get_object_or_404(LeadModel, id = lead_id)
        if instance.status == lead_status:
            return redirect("view_lead")
        elif lead_status == 'Purchase Order Generated':
            project_data = {
                'name': instance.title,
                'technology': "",
                'description': instance.description,
                'files': "",
                'client': instance.client,
                'status': lead_status,
            }
            if create_project(request, project_data) == True:
                # if create_project_resource(request, project_data) == True:
                instance.status = lead_status
                instance.save() # ? or instance.delete()
                return redirect("view_lead")
                # else:
                #     messages.warning(request, "Project created but Unable to update...")
                #     return redirect("view_lead")
            else:
                messages.warning(request, "Unable to save...")
                return redirect("view_lead")
        else:
            instance.status = lead_status
            instance.save()
            messages.success(request,"Lead Updated Successfuly...")
            return redirect("view_lead")
