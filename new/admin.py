from django import forms
from django.contrib import admin
from new.models import *
from .forms import SprintForm


# Register your models here.
admin.site.register(ResourceDetail)
admin.site.register(ClientDetail)
admin.site.register(ProjectDetail)
admin.site.register(ResourceProjectDetail)
admin.site.register(RaisedQueryDetail)
admin.site.register(Attend)
admin.site.register(Blog)
admin.site.register(JobApplicant)
admin.site.register(WebQueries)
admin.site.register(Worklog)
admin.site.register(LeadModel)
admin.site.register(LeadFollowUP)
admin.site.register(Board)
admin.site.register(ResourceBoard)
admin.site.register(Task)
admin.site.register(TaskComment)
admin.site.register(Subscriber)
admin.site.register(Sprint)
admin.site.register(ResourceProgress)
admin.site.register(leaveModel)

