import calendar
import datetime
from django.shortcuts import get_object_or_404, redirect, render
from salary_management.models import EmployeeAccountDetail, EmployeeSalary, Salary
from new.models import LeaveCalendar, ResourceDetail
from salary_management.forms import AddAccountForm, AddEmpSalaryForm, ManualMonthlySalaryForm
from django.contrib import messages
from new.views import pre_month_res_attend_count
from new.leave_model_view import leave_count_of_month
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa  
from num2words import num2words
from new.views import get_user_role



def add_emp_salary(request):
    '''add Annualy salary structure of employee'''
    form = AddEmpSalaryForm(request.POST or None)
    if request.method == "POST":
        
        if form.is_valid():
            '''calculate of basic salary, hra, pf, ta etc'''
            employment_type = request.POST.get('employment_type')
            
            if employment_type == 'New Joining':
                '''Salary calculation if employee new joined '''
                employee_id = request.POST.get('employee')
                employee = ResourceDetail.objects.get(id=employee_id)
                totalSalary = int((form.instance.total_salary).amount) 
                
                if employee.tds:
                    form.instance.pf = totalSalary*10/100
                    form.instance.basic_salary = totalSalary*30/100
                else:
                    form.instance.pf = totalSalary*0/100
                    form.instance.basic_salary = totalSalary*40/100         #40%
                                
                form.instance.hra = totalSalary*22.5/100
                form.instance.conveyance = totalSalary*5.5/100              #68%
                form.instance.special_allowance = totalSalary*25.7/100
                form.instance.fuel = totalSalary*4.3/100                    #98%
                form.instance.ta = totalSalary*0/100
                form.instance.medical = totalSalary*2/100                   #100%
                form.instance.leave_encashment = totalSalary*0/100
                form.instance.telephone = totalSalary*0/100
                form.instance.performence_pay = totalSalary*0/100
                form.instance.arrear = totalSalary*0/100
                form.instance.gratuity = totalSalary*0/100
                form.instance.skill_development = totalSalary*0/100
                
            else:
                '''Salary calculation if employee gets Apprasial'''
                messages.warning(request, "Feature Unavailable: Apprasial of employee Details can't be add...!")
                return render(request, "salary/add_salary.html", {'form': form})
            
            form.save()
            messages.success(request, "Salary Details added succeess...!")
            return redirect('view_emp_salary')
        else:
            messages.warning(request, "Invalied form")
            return render(request, "salary/add_salary.html", {'form': form})
    return render(request, "salary/add_salary.html", {'form': form})
    
    
def view_emp_salary(request):
    '''this function return employee Annual or Monthly salary bases of joining time'''
    context={}
    user = request.user
    user_role = get_user_role(request, user)
    if user_role == "human_resource" or user.is_superuser:
        context['data'] = EmployeeSalary.objects.all()
    else:
        context['data'] = EmployeeSalary.objects.filter(employee=user)
        
    return render(request, 'salary/view_emp_salary.html', context)


def alter_emp_salary(request, id):
    instance = get_object_or_404(EmployeeSalary, id=id)
    form = AddEmpSalaryForm(request.POST or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            
            employee_id = request.POST.get('employee')
            employee = ResourceDetail.objects.get(id=employee_id)
                
            totalSalary = int((form.instance.total_salary).amount) 
                
            if employee.tds:
                form.instance.pf = totalSalary*10/100
                form.instance.basic_salary = totalSalary*30/100
            else:
                form.instance.pf = totalSalary*0/100
                form.instance.basic_salary = totalSalary*40/100         #40%
                            
            form.instance.hra = totalSalary*22.5/100
            form.instance.conveyance = totalSalary*5.5/100              #68%
            form.instance.special_allowance = totalSalary*25.7/100
            form.instance.fuel = totalSalary*4.3/100                    #98%
            form.instance.ta = totalSalary*0/100
            form.instance.medical = totalSalary*2/100                   #100%
            form.instance.leave_encashment = totalSalary*0/100
            form.instance.telephone = totalSalary*0/100
            form.instance.performence_pay = totalSalary*0/100
            form.instance.arrear = totalSalary*0/100
            form.instance.gratuity = totalSalary*0/100
            form.instance.skill_development = totalSalary*0/100
            
            form.save()
            messages.success(request,"Updated..!")
            return redirect('view_emp_salary')
        else:
            messages.warning(request, "Invailied Form..!")
    return render(request, 'salary/update.html', {'form':form, 'title':"Update Salary Detail", 'emp_salary':True})
        
    
def delete_emp_salary(request, id):
    instance = get_object_or_404(EmployeeSalary, id=id)
    if instance:
        instance.delete()
        messages.success(request, "Successfully deleted.. !")
    else:
        messages.warning(request, "Not found.. !")
    return redirect('view_emp_salary')
    
    
def view_salary(request):
    '''this will return employee per month slaries'''
    context={}
    user = request.user
    user_role = get_user_role(request, user)
    if user_role == "human_resource" or user.is_superuser:
        context['salary'] = Salary.objects.all()
    else:
        context['salary'] = Salary.objects.filter(employee=user)
    return render(request, 'salary/view_salary.html', context)


def add_account(request):
    '''this function is used to add new bank account details of employee'''
    form = AddAccountForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, "Account Details added succeess...!")
            return redirect('view_account')
        else:
            messages.warning(request, "Invalied form")
            return render(request, "salary/add_ac.html", {'form': form})
    else:
        return render(request, "salary/add_ac.html", {'form': form})
        
        
def view_account(request):
    context={
        'data':EmployeeAccountDetail.objects.all()
    }
    return render(request, 'salary/view_account.html', context)


def alter_account(request, id):
    instance = get_object_or_404(EmployeeAccountDetail, id=id)
    form = AddAccountForm(request.POST or None, request.FILES or None, instance=instance)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request,"Updated..!")
            return redirect('view_emp_salary')
        else:
            messages.warning(request, "Invailied Form..!")
    return render(request, 'salary/update.html', {'form':form, 'title': "Update Account Details", 'view_url':'view_account', 'account':True})
        
    
def delete_account(request, id):
    instance = get_object_or_404(EmployeeAccountDetail, id=id)
    if instance:
        instance.delete()
        messages.success(request, "Successfully deleted.. !")
    else:
        messages.warning(request, "Not found.. !")
    return redirect('view_account')
    
    
def html_to_pdf(template_src, context_dict={}):
    template = get_template(template_src)
    html  = template.render(context_dict)
    result = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1", 'ignore')), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), content_type='application/pdf')
    return None


def slip(request, id):
    template = 'salary/salary_pdf_template.html'
    salary = get_object_or_404(Salary, id=id)
    total_deduction = salary.pf.amount + salary.deductions.amount
    context = {
        'slip': salary,
        'total_deduction': total_deduction,
        'net_pay': salary.current_month_total_salary.amount - total_deduction,
        # 'amount_hindi':num2words(salary.current_month_total_salary.amount - total_deduction)
    }
    
    pdf = html_to_pdf(template, context)
    return HttpResponse(pdf, content_type='application/pdf')


def add_emp_monthly_salary(request, employee, company_working_day):
    
    emp_working_days = pre_month_res_attend_count(employee)
    deductions = 0
    try:
        employee.employeeaccountdetail  #this is checking employee bank account only
        try:
            annual_salary = EmployeeSalary.objects.get(employee=employee)
        except:
            annual_salary = EmployeeSalary.objects.filter(employee=employee).first()
        per_month_salary = annual_salary.total_salary/12
        per_day_salary = per_month_salary/company_working_day
        if emp_working_days == company_working_day:
            pass
        else:
            pre_month = get_previous_month()
            leaves = leave_count_of_month(employee, pre_month.month, pre_month.year)
            unpaid_leave = leaves['unpaid_leave_taken']
            if unpaid_leave > 0:
                deductions += unpaid_leave*per_day_salary
        
        current_month_total_salary = (emp_working_days*per_day_salary)-deductions
        
        if employee.tds:
            pf = current_month_total_salary*10/100
        else:
            pf = current_month_total_salary*0/100
            
        basic_salary = current_month_total_salary*40/100         #40%
                        
        hra = current_month_total_salary*22.5/100
        conveyance = current_month_total_salary*5.5/100              #68%
        special_allowance = current_month_total_salary*25.7/100
        fuel = current_month_total_salary*4.3/100                    #98%
        ta = current_month_total_salary*0/100
        medical = current_month_total_salary*2/100                   #100%
        leave_encashment = current_month_total_salary*0/100
        telephone = current_month_total_salary*0/100
        performence_pay = current_month_total_salary*0/100
        arrear = current_month_total_salary*0/100
        gratuity = current_month_total_salary*0/100
        skill_development = current_month_total_salary*0/100
               
        Salary.objects.create(employee=employee, total_days=emp_working_days, deductions=deductions,
                              current_month_total_salary=current_month_total_salary, pf=pf,basic_salary=basic_salary,
                              hra=hra,conveyance=conveyance,special_allowance=special_allowance,fuel=fuel,ta=ta,
                              medical=medical,leave_encashment=leave_encashment,telephone=telephone,
                              performence_pay=performence_pay,arrear=arrear,gratuity=gratuity,
                              skill_development=skill_development)
        return True
    except:
        return False


def manual_salary_slip_genaration(request):
    form = ManualMonthlySalaryForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            
            employee = ResourceDetail.objects.get(id=int(request.POST['employee']))
            emp_working_days = int(request.POST['total_working_days'])
            month = request.POST['slip_month'].split('-')
            print(month)
            month = datetime.date(int(month[0]),int(month[1]),int(month[2]))
            
            days_of_month = calendar.monthrange(month.year,month.month)[1]
            sundays = len([1 for i in calendar.monthcalendar(month.year, month.month) if i[6] != 0])
            saturdays = len([1 for i in calendar.monthcalendar(month.year, month.month) if i[5] != 0])
            company_leave = LeaveCalendar.objects.filter(date__month = month.month, date__year=month.year).count()
            company_working_day = days_of_month - sundays - saturdays - company_leave
        
            employee.employeeaccountdetail  #this is checking employee bank account only
            try:
                annual_salary = EmployeeSalary.objects.get(employee=employee)
            except:
                annual_salary = EmployeeSalary.objects.filter(employee=employee).first()
                
            per_month_salary = annual_salary.total_salary/12
            per_day_salary = per_month_salary/company_working_day
            current_month_total_salary = annual_salary.total_salary/12
            deductions = current_month_total_salary*0/100
            
            if emp_working_days == company_working_day:
                pass
            elif emp_working_days > company_working_day:
                emp_working_days = company_working_day
            else:
                unpaid_leave = int(request.POST['unpaid_leave_count'])
                if unpaid_leave > 0:
                    deductions += unpaid_leave*per_day_salary
            
            
            if employee.tds:
                pf = current_month_total_salary*10/100
            else:
                pf = current_month_total_salary*0/100
                
            basic_salary = current_month_total_salary*40/100         #40%
            hra = current_month_total_salary*22.5/100
            conveyance = current_month_total_salary*5.5/100              #68%
            special_allowance = current_month_total_salary*25.7/100
            fuel = current_month_total_salary*4.3/100                    #98%
            ta = current_month_total_salary*0/100
            medical = current_month_total_salary*2/100                   #100%
            leave_encashment = current_month_total_salary*0/100
            telephone = current_month_total_salary*0/100
            performence_pay = current_month_total_salary*0/100
            arrear = current_month_total_salary*0/100
            gratuity = current_month_total_salary*0/100
            skill_development = current_month_total_salary*0/100
            salary = basic_salary+hra+conveyance+special_allowance+fuel+ta+medical+leave_encashment
            
            slip = {
                'employee': employee,
                'get_bank_details':employee.employeeaccountdetail,
                'hra': hra,
                'conveyance': conveyance,
                'special_allowance': special_allowance,
                'fuel': fuel,
                'ta' : ta,
                'medical': medical,
                'leave_encashment' : leave_encashment,
                'telephone' : telephone,
                'performence_pay' : performence_pay,
                'arrear' : arrear,
                'gratuity': gratuity,
                'skill_development' :skill_development,
                'basic_salary' : basic_salary,
                'month': month,
                'pf' : pf,
                'deductions': deductions,
                'total_days': emp_working_days,
                'salary': salary
            }
            
            template = 'salary/salary_pdf_template.html'
            
            pdf = html_to_pdf(template, {'slip':slip,'total_deduction': pf+deductions,'net_pay': salary-(pf+deductions)})
            return HttpResponse(pdf, content_type='application/pdf')
            
        else:
            messages.warning(request, "Invalied form")
            return render(request, "salary/add_manual_salary.html", {'form': form})
    else:
        return render(request, "salary/add_manual_salary.html", {'form': form})
        
    
def previous_month_company_working_day():
    lastMonth = get_previous_month()
    days_of_month = calendar.monthrange(lastMonth.year,lastMonth.month)[1]
    sundays = len([1 for i in calendar.monthcalendar(lastMonth.year, lastMonth.month) if i[6] != 0])
    saturdays = len([1 for i in calendar.monthcalendar(lastMonth.year, lastMonth.month) if i[5] != 0])
    company_leave = LeaveCalendar.objects.filter(date__month = lastMonth.month, date__year=lastMonth.year).count()
    return days_of_month - sundays - saturdays - company_leave


# url set_monthly_salary
def set_monthly_salary(request):
    resources = ResourceDetail.objects.all()
    company_working_day=previous_month_company_working_day()
    salary_not_added = []
    for resource in resources:
        if add_emp_monthly_salary(request, employee=resource, company_working_day=company_working_day):
            pass    #salary slip genarated
        else:
            salary_not_added.append(resource.first_name)    #salary not added
    message = "Salary not added of: ", salary_not_added
    messages.warning(request, message)
    return redirect(view_salary)


def get_previous_month():
    today = datetime.date.today()
    first = today.replace(day=1)
    return first - datetime.timedelta(days=1)


def appointment_letter(request, id):
    salary=get_object_or_404(EmployeeSalary, id=id)
    template = 'salary/appointment.html'
    context={
        'appoint':salary,
        'ceo': "Ankit Singh",
        'basicSalary': int(salary.basic_salary.amount/12),
        'fuel': int(salary.fuel.amount/12),
        'hra': int(salary.hra.amount/12),
        'conveyance': int(salary.conveyance.amount/12),
        'special_allowance': int(salary.special_allowance.amount/12),
        'medical': int(salary.medical.amount/12),
        'pf': int(salary.pf.amount/12),
        'total_month_salary': int(salary.total_salary.amount/12),
    }
    pdf = html_to_pdf(template,context)
    return HttpResponse(pdf, content_type='application/pdf')

