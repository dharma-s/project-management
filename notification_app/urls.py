from django.urls import path, include, re_path
from notification_app.views import view_notification

urlpatterns = [
    path("view_notification/", view_notification, name="notification_page")
]
