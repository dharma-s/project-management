from django.urls import path, re_path
from salary_management import views


urlpatterns = [
    
    re_path(r"^add_emp_salary/$", views.add_emp_salary, name="add_emp_salary"),
    re_path(r"^view_salary/$", views.view_salary, name="view_salary"),
    re_path(r"^view_emp_salary/$", views.view_emp_salary, name="view_emp_salary"),
    re_path(r"^add_account/$", views.add_account, name="add_account"),
    re_path(r"^view_account/$", views.view_account, name="view_account"),
    re_path(r"^slip/(?P<id>\d+)/$", views.slip, name="slip"),
    re_path(r"^appointment_letter/(?P<id>\d+)/$", views.appointment_letter, name="appointment_letter"),
    re_path(r"^delete_emp_salary/(?P<id>\d+)/$", views.delete_emp_salary, name="delete_emp_salary"),
    re_path(r"^alter_emp_salary/(?P<id>\d+)/$", views.alter_emp_salary, name="alter_emp_salary"),
    re_path(r"^alter_account/(?P<id>\d+)/$", views.alter_account, name="alter_account"),
    re_path(r"^delete_account/(?P<id>\d+)/$", views.delete_account, name="delete_account"),
    re_path(r"^set_monthly_salary/$", views.set_monthly_salary, name="set_monthly_salary"),     #auth url added
    re_path(r"^manual-salary/$", views.manual_salary_slip_genaration , name="manual-salary"),     
]