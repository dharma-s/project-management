# Forms for Models
import requests.packages
from django import forms
from django.db.models import fields
from django.forms import ModelChoiceField, FileField, widgets
from django.forms.fields import DateField
from .models import *
from django.utils.translation import ugettext_lazy
from ckeditor.widgets import CKEditorWidget
from django.contrib.auth.models import User


class MyClearableFileInput(forms.ClearableFileInput):
    """ This class will clear the old file when uploading the new """

    clear_checkbox_label = ugettext_lazy('Delete')


# Project Detail Form
class ProjectDetailForm(forms.ModelForm):
    files = FileField(widget=MyClearableFileInput)

    class Meta:
        model = ProjectDetail
        fields = "__all__"


# Client Detail Form
class ClientDetailForm(forms.ModelForm):
    # password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = ClientDetail
        exclude = ("is_admin", "is_active", "is_staff", "is_superuser", "created_by")

    def clean(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return self.cleaned_data

        if ResourceDetail.objects.filter(
                email_id=self.cleaned_data.get("email_id")).exists() or ClientDetail.objects.filter(
            email_id=self.cleaned_data.get("email_id")).exists() or User.objects.filter(
            email=self.cleaned_data.get("email_id")).exists():
            raise forms.ValidationError({"email_id": "Email Already Exists"})
            return self.cleaned_data.get("email_id")
        else:
            pass

    def __init__(self, *args, **kwargs):
        super(ClientDetailForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['email_id'].widget.attrs['readonly'] = True


# Resource Detail Form
class ResourceProjectDetailForm(forms.ModelForm):
    class Meta:
        model = ResourceProjectDetail
        fields = "__all__"


# RQD
class RQDForm(forms.ModelForm):
    files = FileField(widget=MyClearableFileInput)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(RQDForm, self).__init__(*args, **kwargs)
        try:
            email_id = user.email_id
        except:
            email_id = user.email
        if ClientDetail.objects.filter(email_id=email_id).exists():
            client = ClientDetail.objects.get(email_id=email_id)
            selected_project = ProjectDetail.objects.filter(client=client)
            resources = ResourceProjectDetail.objects.filter(project__in=selected_project).values('resource')
            selected_resources = ResourceDetail.objects.filter(pk__in=resources)
            self.fields['resource'] = forms.ModelChoiceField(queryset=selected_resources, required=False)
            self.fields['project'] = forms.ModelChoiceField(queryset=selected_project, required=False)

    class Meta:
        model = RaisedQueryDetail
        fields = "__all__"


# Resource Detail Form
class ResourceDetailForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = ResourceDetail
        exclude = ("is_admin", "is_active", "is_staff", "is_superuser", "empolyee_id")

    def clean(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            return self.cleaned_data

        if ResourceDetail.objects.filter(
                email_id=self.cleaned_data.get("email_id")).exists() or ClientDetail.objects.filter(
            email_id=self.cleaned_data.get("email_id")).exists() or User.objects.filter(
            email=self.cleaned_data.get("email_id")).exists():
            raise forms.ValidationError({"email_id": "Email Already Exists"})
            return self.cleaned_data.get("email_id")
        elif ResourceDetail.objects.filter(empolyee_id=self.cleaned_data.get("empolyee_id")).exists():
            raise forms.ValidationError({"empolyee_id": "Empolyee_id Already Taken"})
            return self.cleaned_data.get("empolyee_id")

    def __init__(self, *args, **kwargs):
        super(ResourceDetailForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['email_id'].widget.attrs['readonly'] = True


# Attend Models Form
class AttendForm(forms.ModelForm):

    def clean(self):
        # additional cleaning here
        cleaned_data = super(AttendForm, self).clean()
        return cleaned_data

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(AttendForm, self).__init__(*args, **kwargs)
        try:
            email_id = user.email_id
        except:
            email_id = user.email

        try:
            ResourceDetail.objects.get(email_id=email_id, role='Business Development Execute')
            bde = True
        except ResourceDetail.DoesNotExist:
            bde = False

        try:

            if bde == True:
                projects = ResourceProjectDetail.objects.filter(resource=ResourceDetail.objects.
                                                                get(email_id=email_id,
                                                                    role="Business Development Execute")).values(
                    'project')
                resources = ResourceProjectDetail.objects.filter(project__in=projects).values('resource')
                selected_resources = ResourceDetail.objects.filter(pk__in=resources, role="Lead Generator")
                selected_project = ProjectDetail.objects.filter(pk__in=projects)
                self.fields['resource'] = forms.ModelChoiceField(queryset=selected_resources, required=False,
                                                                 widget=forms.SelectMultiple)
                self.fields['project'] = forms.ModelChoiceField(queryset=selected_project, required=False)

            elif ResourceDetail.objects.filter(email_id=email_id).exists():
                team = ["Junior Software Engineer", "Frontend Software Engineer", "Frontend Senior Software Engineer",
                        "Backend Software Engineer", "Backend Senior Software Engineer"]
                projects = ResourceProjectDetail.objects.filter(resource=ResourceDetail.objects.
                                                                get(email_id=email_id, role__in=team)).values('project')
                selected_resources = ResourceDetail.objects.filter(email_id=email_id)
                selected_project = ProjectDetail.objects.filter(pk__in=projects)
                self.fields['resource'] = forms.ModelChoiceField(queryset=selected_resources, required=False)
                self.fields['project'] = forms.ModelChoiceField(queryset=selected_project, required=False)

            else:
                ResourceDetail.objects.get(email_id=email_id)
                team_lead = ["Project Manager", "Solution Architect", "Business Analyst", "Backend Team Lead",
                             "Frontend Team Lead"]
                projects = ResourceProjectDetail.objects.filter(resource=ResourceDetail.objects.
                                                                get(email_id=email_id, role__in=team_lead)).values(
                    'project')
                resources = ResourceProjectDetail.objects.filter(project__in=projects).values('resource')
                selected_resources = ResourceDetail.objects.filter(pk__in=resources)
                selected_project = ProjectDetail.objects.filter(pk__in=projects)
                self.fields['resource'] = forms.ModelChoiceField(queryset=selected_resources, required=False,
                                                                 widget=forms.SelectMultiple)
                self.fields['project'] = forms.ModelChoiceField(queryset=selected_project, required=False)


        except ResourceDetail.DoesNotExist:
            querylist = ResourceDetail.objects.all()
            self.fields['resource'] = forms.ModelChoiceField(queryset=querylist, required=False,
                                                             widget=forms.SelectMultiple)

    class Meta:
        model = Attend
        exclude = ('status',)


#  Blog model form

class BlogForm(forms.ModelForm):
    class Meta:
        model = Blog
        fields = "__all__"


# WorkLog Model Form

class WorklogForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(WorklogForm, self).__init__(*args, **kwargs)
        try:
            email_id = user.email_id
        except:
            email_id = user.email

        if ResourceDetail.objects.filter(email_id=email_id).exists():
            projects = ResourceProjectDetail.objects.filter(resource=ResourceDetail.objects.
                                                            get(email_id=email_id)).values('project')
            selected_resources = ResourceDetail.objects.filter(email_id=email_id)
            selected_project = ProjectDetail.objects.filter(pk__in=projects)
            self.fields['resource'] = forms.ModelChoiceField(queryset=selected_resources, required=False)
            self.fields['project'] = forms.ModelChoiceField(queryset=selected_project, required=False)

    class Meta:
        model = Worklog
        fields = "__all__"


# Lead Model Form
class LeadForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(LeadForm, self).__init__(*args, **kwargs)
        try:
            email_id = user.email_id
        except:
            email_id = user.email
        try:
            user = ResourceDetail.objects.get(email_id=email_id, role__in=['Lead Generator', 'Business Development Execute', 'Business Analyst'])
            lg = True
        except ResourceDetail.DoesNotExist:
            lg = False
        try:
            if lg == True:
                client = ClientDetail.objects.filter(created_by=str(user.id))
                self.fields['client'] = forms.ModelChoiceField(queryset=client, required=False)
        except:
            pass

    class Meta:
        model = LeadModel
        fields = ['title', 'expected_amount', 'managed_by', 'client', 'status', 'description']
        exclude = ('added_by',)


# Lead follow up model form
class LeadFollwUpForm(forms.ModelForm):
    class Meta:
        model = LeadFollowUP
        exclude = ('added_by',)

    def __init__(self, *args, **kwargs):
        lead_id = None
        if 'lead_id' in kwargs:
            lead_id = kwargs.pop('lead_id')
        super(LeadFollwUpForm, self).__init__(*args, **kwargs)
        if lead_id is not None:
            selected_lead = LeadModel.objects.filter(pk=lead_id)
            self.fields['lead'] = forms.ModelChoiceField(queryset=selected_lead, required=False)


class ResourceUpdateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = ResourceDetail
        exclude = ("email_id", "is_admin", "is_active", "is_staff", "is_superuser", "empolyee_id", "role","salary","tds",)


class ClientUpdateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = ClientDetail
        exclude = ("email_id", "is_admin", "is_active", "is_staff", "is_superuser",)


#  Update Lead Status Form
class updateLeadStatusForm(forms.ModelForm):
    class Meta:
        model = LeadModel
        fields = ('status',)


# Leave Modal form
class leaveModelForm(forms.ModelForm):
    start_date = DateField(widget=widgets.DateInput(attrs={'type': 'date'}))
    end_date = DateField(widget=widgets.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, **kwargs):
        super(leaveModelForm, self).__init__(*args, **kwargs)

        lead_list = ["Project Manager", "Solution Architect", "Business Analyst", "Backend Team Lead",
                     "Frontend Team Lead", 'HumanResource', ]

        try:
            lead = ResourceDetail.objects.filter(role__in=lead_list)
            self.fields['manager'] = forms.ModelChoiceField(queryset=lead, required=False)

        except ResourceDetail.DoesNotExist:
            pass

    class Meta:
        model = leaveModel
        exclude = ("date_start", "date_end", "status", "comment", "added_by")


# Leave Calendar View
class LeaveCalendarForm(forms.ModelForm):
    class Meta:
        model = LeaveCalendar
        fields = "__all__"


class TaskForm(forms.ModelForm):

    def clean(self):
        # additional cleaning here
        cleaned_data = super(TaskForm, self).clean()
        return cleaned_data

    def __init__(self, *args, **kwargs):
        board = None
        if 'board' in kwargs:
            board = kwargs.pop('board')
        super(TaskForm, self).__init__(*args, **kwargs)

        try:
            sprint = Sprint.objects.filter(end_date__gte=datetime.date.today())
            self.fields['sprint'] = forms.ModelChoiceField(queryset=sprint, required=False)
        except Sprint.DoesNotExist:
            pass

        try:
            if board != None:
                parent_tasks = Task.objects.filter(board=board)
            else:
                parent_tasks = Task.objects.all()
            self.fields['parent_task'] = forms.ModelChoiceField(queryset=parent_tasks, required=False)
        except:
            pass

        try:
            lead_roles = ['Backend Team Lead', 'Frontend Team Lead', 'Project Manager']
            reporter_list = ResourceDetail.objects.filter(role__in=lead_roles)
            self.fields['reporting'] = forms.ModelChoiceField(queryset=reporter_list)
        except:
            pass

        # try:
        #     boards = Board.objects.filter(project=board.project)
        #     self.fields['board'] = forms.ModelChoiceField(queryset=boards, required=False)
        # except:
        #     pass

        try:
            pk_resources = ResourceProjectDetail.objects.filter(project=board.project).values('resource')
            list_of_resource = ResourceDetail.objects.filter(pk__in=pk_resources).exclude(role='HumanResource')
            self.fields['assigned'] = forms.ModelChoiceField(queryset=list_of_resource)
        except:
            pass

    class Meta:
        model = Task
        fields = "__all__"


class ResourcePasswordReset(forms.ModelForm):
    class Meta:
        model = ResourceDetail
        fields = ("password",)


class ClientPasswordReset(forms.ModelForm):
    class Meta:
        model = ClientDetail
        fields = ("password",)


class SprintForm(forms.ModelForm):
    class Meta:
        model = Sprint
        fields = '__all__'
