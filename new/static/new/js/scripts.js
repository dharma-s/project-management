/*!
 * Start Bootstrap - SB Admin v7.0.4 (https://startbootstrap.com/template/sb-admin)
 * Copyright 2013-2021 Start Bootstrap
 * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-sb-admin/blob/master/LICENSE)
 */
// 
// Scripts
// 
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
  });
window.addEventListener('DOMContentLoaded', event => {

    // Toggle the side navigation
    const sidebarToggle = document.body.querySelector('#sidebarToggle');
    if (sidebarToggle) {
        // Uncomment Below to persist sidebar toggle between refreshes
        // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
        //     document.body.classList.toggle('sb-sidenav-toggled');
        // }
        sidebarToggle.addEventListener('click', event => {
            event.preventDefault();
            document.body.classList.toggle('sb-sidenav-toggled');
            localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
        });
    }

});

$(document).on('click', '#viewWorkLog', function(e) {
    var resource_id = $(this).data('resource-id');
    var project_id = $(this).data('project-id');
    var url = $(this).data('url');
    $.ajax({
        url: url,
        type: 'post',
        data: { 'resource_id': resource_id, 'project_id': project_id, 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val() },
        dataType: 'json',
        beforeSend: function() {
            $("#popmodel").modal("show");
        },
        success: function(data) {
            $("#popmodel .modal-body").html(data.worklog);
        }
    });
});
// THIS FUNCTION IS USED TO set placeholder

function setplaceHolder(){
    let element = document.getElementById("update_password");
    element.placeholder = "Enter old password or set new"
}
setplaceHolder()




$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
