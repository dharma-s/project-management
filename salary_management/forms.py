from dataclasses import fields
from django import forms
from salary_management.models import *
from django.forms import widgets
from django.forms.fields import DateField



class AddAccountForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(AddAccountForm, self).__init__(*args, **kwargs)
        
        try:
            account_added = EmployeeAccountDetail.objects.all().values('employee')
            remain_employee = ResourceDetail.objects.all().exclude(pk__in=account_added)
        except:
            remain_employee = ResourceDetail.objects.all()
        
        self.fields['employee'] = forms.ModelChoiceField(queryset= remain_employee, required=False)
             
    class Meta:
        model = EmployeeAccountDetail
        fields = '__all__'



class AddEmpSalaryForm(forms.ModelForm):
    
    def __init__(self, *args, **kwargs):
        super(AddEmpSalaryForm, self).__init__(*args, **kwargs)
        self.fields['last_apprasial_date'].required = False
        self.fields['joining_date'].required = False
           
    joining_date = DateField(widget=widgets.DateInput(attrs={'type': 'date'}))
    last_apprasial_date = DateField(widget=widgets.DateInput(attrs={'type': 'date'}))
    
    class Meta:
        model = EmployeeSalary
        fields = ('employee', 'total_salary', 'employment_type', 'joining_date', 'last_apprasial_date')
        # exclude = ('basic_salary','hra','conveyance','fuel', 'ta','special_allowance','medical','leave_encashment','telephone','performence_pay','arrear','pf','gratuity','skill_development')
        
        

class ManualMonthlySalaryForm(forms.ModelForm):
    
    class DateInput(forms.DateInput):
        input_type = 'date'

    '''printing manual single use salary form'''
    total_working_days = forms.IntegerField(required=True)
    unpaid_leave_count = forms.IntegerField(required=True, initial=0)
    slip_month = DateField(widget=widgets.DateInput(attrs={'type': 'date'}))
    
    class Meta:
        model = Salary
        fields = ('employee','total_working_days','slip_month', 'unpaid_leave_count')
        
        
        
        